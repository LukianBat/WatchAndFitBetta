package com.example.lukianmichael.WatchAndFit;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.lukianmichael.WatchAndFit.data.InformationDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;
import com.example.lukianmichael.WatchAndFit.data.TrainingDbHelper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private LinearLayout mLinearLayout;
    private TrainingDbHelper mDbHelper;
    private InformationDbHelper mDbHelper1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDbHelper1 = new InformationDbHelper(MainActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        displayDatabaseInfo();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        mDbHelper = new TrainingDbHelper(MainActivity.this);
        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment fragment = null;
            Class fragmentClass = null;
            setTitle("Профиль пользователя");
            fragmentClass = BasicActivity.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
        }
    }
/*
 Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, getNoteReport());
                i.putExtra(Intent.EXTRA_SUBJECT, "Отправка результатов");
                i = Intent.createChooser(i, "Отправить с помощью");
                startActivity(i);
*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);

        mLinearLayout = (LinearLayout) findViewById(R.id.LinearLayoutHeader);
        mLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = null;
                Class fragmentClass = null;
                fragmentClass = BasicActivity.class;
                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                setTitle("Профиль пользователя");
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item_osm clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item_osm clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_OCM) {
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = AddActivity.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setTitle("ОСМ");
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            // Handle the camera action
        } else if (id == R.id.nav_graph) {
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = GraphicActivity.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setTitle("Тренировка");
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();

        } else if (id == R.id.nav_setting) {
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = AllSettingActivity.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            setTitle("Настройки");
        } else if (id == R.id.nav_kal) {
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = KalActivity.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setTitle("Расчёт каллорий");
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();

        }else if (id == R.id.nav_info) {
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = InfoActivity.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setTitle("Инструкция");
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();

        }
        else if (id == R.id.nav_share) {
            String date="0";
            int countDay = 0;
            Cursor cursor = this.mDbHelper.getReadableDatabase().query(TrainingContract.Notes.TABLE_NAME, new String[]{"_id",  TrainingContract.OSM.COLUMN_DATE}, null, null, null, null, null);
            try {
                int idColumnIndex = cursor.getColumnIndex("_id");
                int dateColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_DATE);
                if (cursor.getCount() != 0) {
                    while (cursor.moveToNext()) {
                        String currentDate = cursor.getString(dateColumnIndex);
                        if (!currentDate.equals(date)){
                            countDay++;
                            date=currentDate;
                        }
                    }
                }
                else if (cursor.getCount() == 0){
                    Toast.makeText(getApplicationContext(),"Вы еще не сделали ни одной записи тренировок", Toast.LENGTH_SHORT).show();
                }
            } finally {
                cursor.close();
            }
            if (countDay!=0){
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, "Я тренируюсь вместе SmartGlove уже "+countDay+"дней! \n Присоединяйтесь! https://github.com/LukianBat");
                i.putExtra(Intent.EXTRA_SUBJECT, "Отправка результатов");
                i = Intent.createChooser(i, "Поделиться с помощью...");
                startActivity(i);
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


   /* public void OnClickHead(View view) {
        Fragment fragment = null;
        Class fragmentClass = null;
        fragmentClass = BasicActivity.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
    }
    */

    public void displayDatabaseInfo() {
        Cursor cursor = mDbHelper1.getReadableDatabase().query(TrainingContract.Information.TABLE_NAME1, new String[]{"_id", TrainingContract.Information.COLUMN_WEIGHT, TrainingContract.Information.COLUMN_HEIGH, TrainingContract.Information.COLUMN_AGE, TrainingContract.Information.COLUMN_GENDER}, null, null, null, null, "_id DESC");
        try {
            if (cursor.getCount() == 0) {
                Fragment fragment = null;
                Class fragmentClass = null;
                fragmentClass = SettingActivity.class;
                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                setTitle("Настройки");
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            }
            else {
                Fragment fragment = null;
                Class fragmentClass = null;
                setTitle("Профиль пользователя");
                fragmentClass = BasicActivity.class;
                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            }
        } finally {
            cursor.close();
        }
    }
}
