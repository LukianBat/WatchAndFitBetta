package com.example.lukianmichael.WatchAndFit;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.lukianmichael.WatchAndFit.data.InformationDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;

public class BasicActivity extends Fragment {
    private Button mButton;
    private Button mButton1;
    private Button mButton2;
    private TextView mTextViewWeight;
    private TextView mTextViewHeight;
    private TextView mTextViewAge;
    private TextView mTextViewGender;
    private InformationDbHelper mDbHelper1;
    FragmentTransaction fTrans;
    SettingActivity frag1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =  inflater.inflate(R.layout.activity_basic, container, false);
//
//        ActionBar toolbar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        mButton1 = (Button) rootView.findViewById(R.id.button8);
        mButton = (Button) rootView.findViewById(R.id.button7);
        mButton2= (Button) rootView.findViewById(R.id.button9);
        mTextViewWeight = (TextView) rootView.findViewById(R.id.textViewWeight);
        mTextViewHeight = (TextView) rootView.findViewById(R.id.textViewHeight);
        mTextViewAge = (TextView) rootView.findViewById(R.id.textViewAge);
        mTextViewGender = (TextView) rootView.findViewById(R.id.textViewGender);
        mDbHelper1 = new InformationDbHelper(getActivity());
        frag1 = new SettingActivity();
        displayDatabaseInfo();
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fTrans = getFragmentManager().beginTransaction();
                fTrans.replace(R.id.container, frag1);
                fTrans.commit();
            }
        });
        mButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RecycleViewActivityOsm.class);
                startActivity(intent);
            }
        });
        mButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getActivity(),RecycleViewActivityTr.class);
                startActivity(intent);
            }
        });
        return rootView;
    }
    public void displayDatabaseInfo() {
        Cursor cursor = mDbHelper1.getReadableDatabase().query(TrainingContract.Information.TABLE_NAME1, new String[]{"_id", TrainingContract.Information.COLUMN_WEIGHT, TrainingContract.Information.COLUMN_HEIGH, TrainingContract.Information.COLUMN_AGE, TrainingContract.Information.COLUMN_GENDER}, null, null, null, null, "_id DESC");
        try {
            int idColumnIndex = cursor.getColumnIndex("_id");
            int weightColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_WEIGHT);
            int heighColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_HEIGH);
            int ageColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_AGE);
            int genderColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_GENDER);
            while (cursor.moveToNext()) {
                int currentID = cursor.getInt(idColumnIndex);
                int currentWeight = cursor.getInt(weightColumnIndex);
                int currentHeigh = cursor.getInt(heighColumnIndex);
                int currentAge = cursor.getInt(ageColumnIndex);
                int currentGender = cursor.getInt(genderColumnIndex);
                mTextViewWeight.setText("Вес: "+currentWeight);
                mTextViewHeight.setText("Рост: "+currentHeigh);
                mTextViewAge.setText("Возраст: "+currentAge);
                if (currentGender == 1) {
                    mTextViewGender.setText("Пол: мужской");
                }
                if (currentGender == 2) {
                    mTextViewGender.setText("Пол: женский");
                }
            }
        } finally {
            cursor.close();
        }
    }
}
