package com.example.lukianmichael.WatchAndFit;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lukianmichael.WatchAndFit.data.TrainingContract;
import com.example.lukianmichael.WatchAndFit.data.TrainingDbHelper;

public class ResultsActivity extends Fragment {
    private TextView displayTextView;
    FragmentTransaction fTrans;
    BasicActivity frag1;
    private TrainingDbHelper mDbHelper;
    private Button okButton;
    String dateNote;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item_osm clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.share) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_TEXT, dateNote);
            i.putExtra(Intent.EXTRA_SUBJECT, "Отправка результатов");
            i = Intent.createChooser(i, "Отправить с помощью");
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.activity_results, container, false);
        mDbHelper = new TrainingDbHelper(getActivity());
        okButton = (Button) rootView.findViewById(R.id.button6);
        displayTextView = (TextView) rootView.findViewById(R.id.textView10);
        displayDatabaseInfo();
        frag1 = new BasicActivity();
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResultsActivity.this.fTrans = ResultsActivity.this.getFragmentManager().beginTransaction();
                ResultsActivity.this.fTrans.replace(R.id.container, ResultsActivity.this.frag1);
                ResultsActivity.this.fTrans.commit();
                Toast.makeText(getContext(),"Данные сохранены", Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;

    }
    public void displayDatabaseInfo() {
        Cursor cursor = this.mDbHelper.getReadableDatabase().query(TrainingContract.Notes.TABLE_NAME, new String[]{"_id", TrainingContract.Notes.COLUMN_PULSE, TrainingContract.Notes.COLUMN_TIME, TrainingContract.Notes.COLUMN_ENERGY, TrainingContract.Notes.COLUMN_DATE}, null, null, null, null, "_id DESC");
        try {
            int idColumnIndex = cursor.getColumnIndex("_id");
            int pulseColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_PULSE);
            int timeColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_TIME);
            int energyColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_ENERGY);
            int dateColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_DATE);
            if (cursor.moveToNext()) {
                int currentID = cursor.getInt(idColumnIndex);
                this.displayTextView.append("Ваш средний пульс за тренировку: " + cursor.getInt(pulseColumnIndex) + " уд. в мин;.\nВремя измерений: " + String.format("%.2f",((cursor.getInt(timeColumnIndex))/60.0)) + " мин.;\nКоличество потраченной энергии: " +String.format("%.2f",((cursor.getInt(energyColumnIndex)) / 1000.0)) + " Ккал.\n"+"Дата: "+cursor.getString(dateColumnIndex));
                dateNote="Запись тренировки \n"+"Дата: "+cursor.getString(dateColumnIndex)+"\nВаш средний пульс за тренировку: " + cursor.getInt(pulseColumnIndex) + " уд. в мин;\nВремя измерений: " + String.format("%.2f",((cursor.getInt(timeColumnIndex))/60.0)) + " мин;\nКоличество потраченной энергии: " +String.format("%.2f",((cursor.getInt(energyColumnIndex)) / 1000.0)) + " Ккал";
            }
            cursor.close();
        } catch (Throwable th) {
            cursor.close();
        }
    }
}
