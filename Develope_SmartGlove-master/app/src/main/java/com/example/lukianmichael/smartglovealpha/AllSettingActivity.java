package com.example.lukianmichael.WatchAndFit;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lukianmichael.WatchAndFit.data.OsmDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;
import com.example.lukianmichael.WatchAndFit.data.TrainingDbHelper;

public class AllSettingActivity extends Fragment {
    private OsmDbHelper mDbHelper1;
    private TrainingDbHelper mDbHelper2;
    FragmentTransaction fTrans;
    SettingActivity frag1;
    private TextView mTextViewDelete;
    private TextView mTextViewRate;
    private TextView mTextViewVersion;
    private TextView mTextViewAboutVersion;
    private TextView mTextViewEdit;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView =  inflater.inflate(R.layout.activity_all_setting, container, false);
        mDbHelper1 = new OsmDbHelper(getActivity());
        mDbHelper2 = new TrainingDbHelper(getActivity());
        mTextViewDelete = (TextView) rootView.findViewById(R.id.textViewDelete);
        mTextViewRate = (TextView) rootView.findViewById(R.id.textViewRate);
        mTextViewVersion = (TextView) rootView.findViewById(R.id.textViewVersion);
        mTextViewAboutVersion = (TextView) rootView.findViewById(R.id.textViewAboutVersion);
        mTextViewEdit = (TextView) rootView.findViewById(R.id.textViewEdit);
        frag1 = new SettingActivity();
        mTextViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeAll();
                Toast.makeText(getContext(),"Все записи удалены!" +
                        "", Toast.LENGTH_SHORT).show();
            }
        });
        mTextViewAboutVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),AboutVersion.class);
                startActivity(intent);
            }
        });
        mTextViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fTrans = getFragmentManager().beginTransaction();
                fTrans.replace(R.id.container, frag1);
                fTrans.commit();
            }
        });

        return rootView;
    }
    public void removeAll()
    {
        // db.delete(String tableName, String whereClause, String[] whereArgs);
        // If whereClause is null, it will delete all rows.
        SQLiteDatabase db = mDbHelper1.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(TrainingContract.OSM.TABLE_NAME2, null, null);
        SQLiteDatabase db1 = mDbHelper2.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db1.delete(TrainingContract.Notes.TABLE_NAME, null, null);
    }
}
