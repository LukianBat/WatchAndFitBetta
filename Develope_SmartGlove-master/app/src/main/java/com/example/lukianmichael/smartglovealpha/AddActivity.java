package com.example.lukianmichael.WatchAndFit;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lukianmichael.WatchAndFit.data.OsmDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;

import java.io.OutputStream;
import java.util.UUID;

public class AddActivity extends Fragment {
    private EditText mPsit;
    private EditText mPstand;
    float Result;
    private ImageView mImageView;
    private TextView mResult,mZone;
    private OsmDbHelper mDbHelper;
    int Psit, Pstand,Zone;
    String date;
    //////////
    TextView MacDevice;
    private static final int REQUEST_ACCESS_COARSE_LOCATION = 1;
    private static final int REQUEST_ENABLE_BT = 1;
    final int RECIEVE_MESSAGE = 1;
    private BluetoothSocket btSocket = null;
    private OutputStream outStream = null;
    private BluetoothAdapter bluetoothAdapter;
    private BroadcastReceiver discoverDevicesReceiver;
    private BroadcastReceiver discoveryFinishedReceiver;
    private ProgressDialog progressDialog;
    private static String address;
    private static final UUID MY_UUID = UUID.fromString("ba698752-21d0-4534-9e50-10b2a25bc583");
    private static final String TAG = "bluetooth1";
    private StringBuilder sb = new StringBuilder();
    public Handler h;
    //private ConnectedThreadOSM mConnectedThreadOSM;
    TextView mPulseArduino;
    ///////
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item_osm clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.share) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_TEXT, sendData());
            i.putExtra(Intent.EXTRA_SUBJECT, "Отправка результатов");
            i = Intent.createChooser(i, "Отправить с помощью");
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =  inflater.inflate(R.layout.activity_add, container, false);
        MacDevice = (TextView) rootView.findViewById(R.id.textViewDeviceMac);
        mPulseArduino = (TextView) rootView.findViewById(R.id.textViewPulse);
        ///////////////////////////////////

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            BluetoothManager mbluetoothManager = (BluetoothManager) getActivity().getSystemService(Context.BLUETOOTH_SERVICE); // на версиях начиная JELLY_BEAN_MR2 д
            bluetoothAdapter = mbluetoothManager.getAdapter();
        } else {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        String status = null;
        if(bluetoothAdapter.isEnabled()){
            String mydeviceaddress= bluetoothAdapter.getAddress();
            String mydevicename= bluetoothAdapter.getName();
            status="Ваши данные:"+mydevicename+" : "+ mydeviceaddress;
            Toast.makeText(getContext(), status, Toast.LENGTH_SHORT).show();
        }
        else
        {Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        /////////////////////////////
        //SET!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!/////////////////////////////////
        h = new Handler() {
            public void handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case RECIEVE_MESSAGE:                                                   // если приняли сообщение в Handler
                        byte[] readBuf = (byte[]) msg.obj;
                        String strIncom = new String(readBuf, 0, msg.arg1);
                        sb.append(strIncom);                                                // формируем строку
                        int endOfLineIndex = sb.indexOf("\r\n");                            // определяем символы конца строки
                        //if (endOfLineIndex > 0) {                                            // если встречаем конец строки,
                        String sbprint = sb.substring(0, endOfLineIndex);               // то извлекаем строку
                        sb.delete(0, sb.length());                                      // и очищаем sb
                        mPulseArduino.setText("Ответ от Arduino: " + sbprint);             // обновляем TextView
                        //}
                        //Log.d(TAG, "...Строка:"+ sb.toString() +  "Байт:" + msg.arg1 + "...");
                        break;
                }
            };
        };
        //endSET!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//////////////////////////////////
        mPsit = (EditText) rootView.findViewById(R.id.editText);
        mPstand = (EditText) rootView.findViewById(R.id.editText2);
        mResult = (TextView) rootView.findViewById(R.id.textView6);
        mZone = (TextView) rootView.findViewById(R.id.textView9);
        final Button mButton = (Button) rootView.findViewById(R.id.button);
        mImageView = (ImageView) rootView.findViewById(R.id.imageView);
        mDbHelper = new OsmDbHelper(getActivity());
        Calendar c=Calendar.getInstance();
        if ((c.get(c.MONTH)+1)<10) {
            date = String.valueOf(c.get(c.DAY_OF_MONTH)) + ":0" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
        }
        if (c.get(c.DAY_OF_MONTH)<10) {
            date = "0"+String.valueOf(c.get(c.DAY_OF_MONTH)) + ":" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
        }
        if ((c.get(c.MONTH)+1)<10) {
            date = String.valueOf(c.get(c.DAY_OF_MONTH)) + ":0" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
        }
        if ((c.get(c.DAY_OF_MONTH)<10) && ((c.get(c.MONTH)+1)<10)) {
            date = "0"+String.valueOf(c.get(c.DAY_OF_MONTH)) + ":0" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
        }
        mPstand.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mButton.setClickable(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mPsit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mButton.setClickable(true);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (mPsit.getText().toString().equals("") || mPstand.getText().toString().equals("")) {
                    mResult.setText("Введите значения!");
                    mZone.setText("");
                }
                else {
                    Psit = Integer.parseInt(mPsit.getText().toString());
                    Pstand = Integer.parseInt(mPstand.getText().toString());
                    Result = (float) (14.5-0.5*(Psit-40)/3.5-(Pstand-Psit)/2.27*0.5);
                    mResult.setText("Балл:  "+String.format("%.2f",Result));
                    if (Result>7.5){
                        Zone=1;
                        mImageView.setImageResource(R.drawable.one_green);
                        mZone.setText("зона");
                    }
                    if (Result<=7.5 && Result>4.5){
                        Zone=2;
                        mImageView.setImageResource(R.drawable.two_blue);
                        mZone.setText("зона");
                    }
                    if (Result<=4.5 && Result>=2){
                        Zone=3;
                        mImageView.setImageResource(R.drawable.three_yellow);
                        mZone.setText("зона");
                    }
                    if (Result<2){
                        Zone=4;
                        mImageView.setImageResource(R.drawable.four_red);
                        mZone.setText("зона");
                    }
                }
                insertOsm();
                Toast.makeText(getContext(),"Данные сохранены", Toast.LENGTH_SHORT).show();
                mButton.setClickable(false);
        }
        });
        return rootView;
    }
    private String sendData(){
        String sendNote = "Запись ОСМ "+String.valueOf(date)+";\n" +" ЧСС сидя: " +String.valueOf(Psit)+";\n" + " ЧСС стоя: "+String.valueOf(Pstand)+";\n"+ " Балл: "+String.format("%.2f",Result)+";\n"+" Зона: "+String.valueOf(Zone);
        return sendNote;
    }
    private void insertOsm() {
        SQLiteDatabase db = this.mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TrainingContract.OSM.COLUMN_PSIT, Integer.valueOf(this.Psit));
        values.put(TrainingContract.OSM.COLUMN_PSTAND, Integer.valueOf(this.Pstand));
        values.put(TrainingContract.OSM.COLUMN_POINT, Float.valueOf(this.Result));
        values.put(TrainingContract.OSM.COLUMN_ZONE, Integer.valueOf(this.Zone));
        values.put(TrainingContract.OSM.COLUMN_DATE,String.valueOf(this.date));
        long newRowId = db.insert(TrainingContract.OSM.TABLE_NAME2, null, values);
    }
    public void discoverDevices() {
        if (discoverDevicesReceiver == null) {
            discoverDevicesReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();

                    if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                        if (String.valueOf(device.getName()).equals("S4505M")){
                            MacDevice.setText(String.valueOf(device.getAddress()));
                            address=String.valueOf(device.getAddress());
                            progressDialog.dismiss();
                        }
                    }
                }


            };
        }
        if (discoveryFinishedReceiver == null) {
            discoveryFinishedReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (progressDialog != null)
                        progressDialog.dismiss();
                    Toast.makeText(getContext(), "Устройства не найдено", Toast.LENGTH_LONG).show();
                    getActivity().unregisterReceiver(discoveryFinishedReceiver);
                }
            };
        }
        getActivity().registerReceiver(discoverDevicesReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        getActivity().registerReceiver(discoveryFinishedReceiver, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));
        progressDialog = ProgressDialog.show(getActivity(), "Поиск устройств", "Подождите...");
        bluetoothAdapter.startDiscovery();

    }
}
