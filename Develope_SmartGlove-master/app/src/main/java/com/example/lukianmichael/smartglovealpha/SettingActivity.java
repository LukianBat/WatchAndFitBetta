package com.example.lukianmichael.WatchAndFit;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.lukianmichael.WatchAndFit.data.InformationDbHelper;
import com.example.lukianmichael.WatchAndFit.data.OsmDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;
import com.example.lukianmichael.WatchAndFit.data.TrainingDbHelper;

public class SettingActivity extends Fragment {
    private EditText EditTextAge;
    private EditText EditTextHeigh;
    private EditText EditTextWeight;
    int Gender;
    // private TextView display;
    //private TextView display1;
    private Button mButton;
    public TrainingDbHelper mDbHelper;
    private InformationDbHelper mDbHelper1;
    private OsmDbHelper mDbHelper2;
    RadioButton mradioButtonM;
    RadioButton mradioButtonW;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =  inflater.inflate(R.layout.activity_setting, container, false);
        RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.RadioGroup);
        mradioButtonM = (RadioButton) rootView.findViewById(R.id.radioButtonM);
        mradioButtonW = (RadioButton) rootView.findViewById(R.id.radioButtonW);
        //display = (TextView) rootView.findViewById(R.id.textView25);
        //display1 = (TextView) rootView.findViewById(R.id.textView26);
        mButton = (Button) rootView.findViewById(R.id.button7);
        EditTextWeight = (EditText) rootView.findViewById(R.id.editTextWeight);
        EditTextHeigh = (EditText) rootView.findViewById(R.id.editTextHeigh);
        EditTextAge = (EditText) rootView.findViewById(R.id.editTextAge);
        mDbHelper = new TrainingDbHelper(getActivity());
        mDbHelper1 = new InformationDbHelper(getActivity());
        mDbHelper2 = new OsmDbHelper(getActivity());
        //displayDatabaseInfoTraining();
        //displayDatabaseInfoOsm();
        displayDatabaseInfo();
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButtonM: Gender = 1;
                        break;
                    case R.id.radioButtonW: Gender =2;
                        break;
                    default:
                        break;
                }
            }
        });
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateInf();
                displayDatabaseInfo();
                Toast.makeText(getContext(),"Данные изменены", Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }
    /*public void displayDatabaseInfoTraining() {
        Cursor cursor = this.mDbHelper.getReadableDatabase().query(TrainingContract.Notes.TABLE_NAME, new String[]{"_id", TrainingContract.Notes.COLUMN_PULSE, TrainingContract.Notes.COLUMN_TIME, TrainingContract.Notes.COLUMN_ENERGY}, null, null, null, null, null);
        try {
            display.setText(cursor.getCount() + " измерений\n\n");
            display.append("Номер - Пульс - Время - Энергозатратность\n");
            int idColumnIndex = cursor.getColumnIndex("_id");
            int pulseColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_PULSE);
            int timeColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_TIME);
            int energyColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_ENERGY);
            while (cursor.moveToNext()) {
                int currentID = cursor.getInt(idColumnIndex);
                int currentPulse = cursor.getInt(pulseColumnIndex);
                int currentTime = cursor.getInt(timeColumnIndex);
                display.append("\n" + currentID + " - " + currentPulse + " уд - " + currentTime + " сек - " + cursor.getInt(energyColumnIndex) + " Кал ");
            }
        } finally {
            cursor.close();
        }
    }
    public void displayDatabaseInfoOsm() {
        Cursor cursor = this.mDbHelper2.getReadableDatabase().query(TrainingContract.OSM.TABLE_NAME2, new String[]{"_id", TrainingContract.OSM.COLUMN_PSIT, TrainingContract.OSM.COLUMN_PSTAND, TrainingContract.OSM.COLUMN_POINT, TrainingContract.OSM.COLUMN_ZONE}, null, null, null, null, null);
        try {
            display1.setText(cursor.getCount() + " измерений\n\n");
            display1.append("Номер - ЧСС сидя - ЧСС стоя - Балл - Зона\n");
            int idColumnIndex = cursor.getColumnIndex("_id");
            int psitColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_PSIT);
            int pstandColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_PSTAND);
            int pointColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_POINT);
            int zoneColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_ZONE);
            while (cursor.moveToNext()) {
                int currentID = cursor.getInt(idColumnIndex);
                int currentPsit = cursor.getInt(psitColumnIndex);
                int currentPstand = cursor.getInt(pstandColumnIndex);
                int currentPoint = cursor.getInt(pointColumnIndex);
                display1.append("\n" + currentID + " - " + currentPsit + " уд - " + currentPstand + " уд - " + currentPoint + " баллов - " + cursor.getInt(zoneColumnIndex) + " зона ");
            }
        } finally {
            cursor.close();
        }
    }
    */
    public void displayDatabaseInfo() {
        Cursor cursor = mDbHelper1.getReadableDatabase().query(TrainingContract.Information.TABLE_NAME1, new String[]{"_id", TrainingContract.Information.COLUMN_WEIGHT, TrainingContract.Information.COLUMN_HEIGH, TrainingContract.Information.COLUMN_AGE, TrainingContract.Information.COLUMN_GENDER}, null, null, null, null, "_id DESC");
        try {
            if (cursor.getCount() == 0) {
                insertInf();
                displayDatabaseInfo();
            }
            int idColumnIndex = cursor.getColumnIndex("_id");
            int weightColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_WEIGHT);
            int heighColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_HEIGH);
            int ageColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_AGE);
            int genderColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_GENDER);
            while (cursor.moveToNext()) {
                int currentID = cursor.getInt(idColumnIndex);
                int currentWeight = cursor.getInt(weightColumnIndex);
                int currentHeigh = cursor.getInt(heighColumnIndex);
                int currentAge = cursor.getInt(ageColumnIndex);
                int currentGender = cursor.getInt(genderColumnIndex);
                EditTextWeight.setText(currentWeight + "");
                EditTextHeigh.setText(currentHeigh + "");
                EditTextAge.setText(currentAge + "");
                if (currentGender == 1) {
                    this.mradioButtonM.setChecked(true);
                }
                if (currentGender == 2) {
                    this.mradioButtonW.setChecked(true);
                }
            }
        } finally {
            cursor.close();
        }
    }
    private void UpdateInf() {
        SQLiteDatabase db = this.mDbHelper1.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TrainingContract.Information.COLUMN_WEIGHT, Integer.valueOf(Integer.parseInt(this.EditTextWeight.getText().toString())));
        values.put(TrainingContract.Information.COLUMN_HEIGH, Integer.valueOf(Integer.parseInt(this.EditTextHeigh.getText().toString())));
        values.put(TrainingContract.Information.COLUMN_AGE, Integer.valueOf(Integer.parseInt(this.EditTextAge.getText().toString())));
        if (this.mradioButtonM.isChecked()) {
            values.put(TrainingContract.Information.COLUMN_GENDER, Integer.valueOf(1));
        }
        if (this.mradioButtonW.isChecked()) {
            values.put(TrainingContract.Information.COLUMN_GENDER, Integer.valueOf(2));
        }
        long newRowId = (long) db.update(TrainingContract.Information.TABLE_NAME1, values, null, null);
    }

    private void insertInf() {
        SQLiteDatabase db = this.mDbHelper1.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TrainingContract.Information.COLUMN_WEIGHT, Integer.valueOf(0));
        values.put(TrainingContract.Information.COLUMN_HEIGH, Integer.valueOf(0));
        values.put(TrainingContract.Information.COLUMN_AGE, Integer.valueOf(0));
        values.put(TrainingContract.Information.COLUMN_GENDER, Integer.valueOf(0));
        long newRowId = db.insert(TrainingContract.Information.TABLE_NAME1, null, values);
    }
}
