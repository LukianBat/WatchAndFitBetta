package com.example.lukianmichael.WatchAndFit;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lukianmichael.WatchAndFit.data.InformationDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;
import com.example.lukianmichael.WatchAndFit.data.TrainingDbHelper;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
import java.util.UUID;

public class GraphicActivity extends Fragment {

    //////////
    TextView txtArduino;
    private static final int REQUEST_ACCESS_COARSE_LOCATION = 1;
    private static final int REQUEST_ENABLE_BT = 1;
    final int RECIEVE_MESSAGE = 1;
    private BluetoothSocket btSocket = null;
    private OutputStream outStream = null;
    private BluetoothAdapter bluetoothAdapter;
    private BroadcastReceiver discoverDevicesReceiver;
    private BroadcastReceiver discoveryFinishedReceiver;
    private ProgressDialog progressDialog;
    private static String address;
    private static final UUID MY_UUID = UUID.fromString("ba698752-21d0-4534-9e50-10b2a25bc583");
    private static final String TAG = "bluetooth1";
    private StringBuilder sb = new StringBuilder();
    public Handler h;
    private ConnectedThread mConnectedThread;
    ///////
    double srPulse=0;
    String date;
    long Time = 0;
    TextView mTextView;
    private Chronometer mChronometer;
    ResultsActivity frag1;
    FragmentTransaction fTrans;
    long TimePause=0;
    private Button mButton;
    private Button mButton1;
    private Button mButton2;
    private TrainingDbHelper mDbHelper;
    private InformationDbHelper mDbHelper1;
    private TextView mDevices;
    int x=0;
    int y=0;
    int x0=0;
    int y0=0;
    int weight;
    GraphView graph;
    @RequiresApi(api = Build.VERSION_CODES.N)
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_graphic, container, false);
        graph = (GraphView) rootView.findViewById(R.id.graph);
        txtArduino = (TextView) rootView.findViewById(R.id.textView28);
        mDevices = (TextView)rootView.findViewById(R.id.textView27);
        mTextView = (TextView)rootView.findViewById(R.id.textView20);
        ///////////////////////////////////

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            BluetoothManager mbluetoothManager = (BluetoothManager) getActivity().getSystemService(Context.BLUETOOTH_SERVICE); // на версиях начиная JELLY_BEAN_MR2 д
            bluetoothAdapter = mbluetoothManager.getAdapter();
        } else {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        String status = null;
        if(bluetoothAdapter.isEnabled()){
            String mydeviceaddress= bluetoothAdapter.getAddress();
            String mydevicename= bluetoothAdapter.getName();
            status="Ваши данные:"+mydevicename+" : "+ mydeviceaddress;
            Toast.makeText(getContext(), status, Toast.LENGTH_SHORT).show();
        }
        else
        {Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        /////////////////////////////
        //SET!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!/////////////////////////////////
        h = new Handler() {
            public void handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case RECIEVE_MESSAGE:                                                   // если приняли сообщение в Handler
                        byte[] readBuf = (byte[]) msg.obj;
                        String strIncom = new String(readBuf, 0, msg.arg1);
                        sb.append(strIncom);                                                // формируем строку
                        int endOfLineIndex = sb.indexOf("\r\n");                            // определяем символы конца строки
                        //if (endOfLineIndex > 0) {                                            // если встречаем конец строки,
                            String sbprint = sb.substring(0, endOfLineIndex);               // то извлекаем строку
                            sb.delete(0, sb.length());                                      // и очищаем sb
                            txtArduino.setText("Ответ от Arduino: " + sbprint);             // обновляем TextView
                        //}
                        //Log.d(TAG, "...Строка:"+ sb.toString() +  "Байт:" + msg.arg1 + "...");
                        break;
                }
            };
        };
        //endSET!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//////////////////////////////////
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(0,0)
        });
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(50);

        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(10);

        // enable scaling and scrolling
        graph.getViewport().setScalable(true);
        graph.getViewport().setScalableY(false);
        graph.addSeries(series);
        mChronometer = (Chronometer) rootView.findViewById(R.id.chronometer);
        mButton = (Button)rootView.findViewById(R.id.button2);
        mButton1 = (Button) rootView.findViewById(R.id.button5);
        mButton2 = (Button) rootView.findViewById(R.id.button4);
        discoverDevices();
        mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                ////////////////
                h = new Handler() {
                    public void handleMessage(android.os.Message msg) {
                        switch (msg.what) {
                            case RECIEVE_MESSAGE:                                                   // если приняли сообщение в Handler
                                byte[] readBuf = (byte[]) msg.obj;
                                String strIncom = new String(readBuf, 0, msg.arg1);
                                sb.append(strIncom);                                                // формируем строку
                                int endOfLineIndex = sb.indexOf("\r\n");                            // определяем символы конца строки
                                //if (endOfLineIndex > 0) {                                            // если встречаем конец строки,
                                String sbprint = sb.substring(0, endOfLineIndex);               // то извлекаем строку
                                sb.delete(0, sb.length());                                      // и очищаем sb
                                txtArduino.setText("Ответ от Arduino: " + sbprint);             // обновляем TextView
                                //}
                                //Log.d(TAG, "...Строка:"+ sb.toString() +  "Байт:" + msg.arg1 + "...");
                                break;
                        }
                    };
                };
                ///////////////
                long elapsedMillis = SystemClock.elapsedRealtime()
                        - mChronometer.getBase();
                LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[]{
                        new DataPoint(x0,y0),
                        new DataPoint(x,y)
                });
                graph.getViewport().setYAxisBoundsManual(true);
                graph.getViewport().setMinY(0);
                graph.getViewport().setMaxY(200);

                graph.getViewport().setXAxisBoundsManual(true);
                graph.getViewport().setMinX(0);
                graph.getViewport().setMaxX(10);

                // enable scaling and scrolling
                graph.getViewport().setScalable(true);
                graph.getViewport().setScalableY(false);
                graph.addSeries(series);
                graph.getViewport().setXAxisBoundsManual(true);
                if (x<5){
                    graph.getViewport().setMinX(0);
                    graph.getViewport().setMaxX(x+5);
                }
                else {
                    graph.getViewport().setMinX(x - 5);
                    graph.getViewport().setMaxX(x + 5);
                }
                x0=x;
                y0=y;
                x++;
                y= new Random().nextInt(60)+100;
                Time=elapsedMillis/1000;
                mTextView.setText("Время: "+(TimePause+Time+1)+" Средний пульс: "+((int)(srPulse/(TimePause+Time+1))));
                srPulse=srPulse+y;
            }
        });
        mDbHelper1 = new InformationDbHelper(getActivity());
        mDbHelper = new TrainingDbHelper(getActivity());
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChronometer.setBase(SystemClock.elapsedRealtime());
                mChronometer.start();
                mButton.setClickable(false);
            }
        });
        mButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mButton.setClickable(true);
                mChronometer.stop();
                TimePause+=Time;
                Time=0;
            }
        });
        frag1 = new ResultsActivity();//2141
        mButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChronometer.stop();
                Time++;
                Time=TimePause+Time;
                srPulse/=Time;
                //DataBase
                insertTraining();
                fTrans = getFragmentManager().beginTransaction();
                fTrans.replace(R.id.container, frag1);
                fTrans.commit();
                //
            }
        });
        Calendar c=Calendar.getInstance();
        if ((c.get(c.MONTH)+1)<10) {
            date = String.valueOf(c.get(c.DAY_OF_MONTH)) + ":0" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
        }
        if (c.get(c.DAY_OF_MONTH)<10) {
            date = "0"+String.valueOf(c.get(c.DAY_OF_MONTH)) + ":" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
        }
        if ((c.get(c.MONTH)+1)<10) {
            date = String.valueOf(c.get(c.DAY_OF_MONTH)) + ":0" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
        }
        if ((c.get(c.DAY_OF_MONTH)<10) && ((c.get(c.MONTH)+1)<10)) {
            date = "0"+String.valueOf(c.get(c.DAY_OF_MONTH)) + ":0" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
        }
        return rootView;
    }
    public void sendWeight() {
        Cursor cursor = this.mDbHelper1.getReadableDatabase().query(TrainingContract.Information.TABLE_NAME1, new String[]{"_id", TrainingContract.Information.COLUMN_WEIGHT, TrainingContract.Information.COLUMN_HEIGH, TrainingContract.Information.COLUMN_AGE}, null, null, null, null, null);
        try {
            int weightColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_WEIGHT);
            if (cursor.moveToNext()) {
                weight = cursor.getInt(weightColumnIndex);
            }
            cursor.close();
        } catch (Throwable th) {
            cursor.close();
        }
    }
    private void insertTraining() {
        sendWeight();
        SQLiteDatabase db = this.mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TrainingContract.Notes.COLUMN_PULSE, Double.valueOf(srPulse));
        values.put(TrainingContract.Notes.COLUMN_TIME, Float.valueOf(Time));
        values.put(TrainingContract.Notes.COLUMN_ENERGY, Double.valueOf(1000.0 * (((0.014 * (weight)) * (Time/60.0)) * ((0.12 * srPulse) - 7.0))));
        values.put(TrainingContract.Notes.COLUMN_DATE,String.valueOf(date));
        long newRowId = db.insert(TrainingContract.Notes.TABLE_NAME, null, values);
    }
    public void makeDiscoverable() {

    }

    public void discoverDevices() {
/*
        if (ActivityCompat.checkSelfPermission(MainActivity.this, BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{ACCESS_FINE_LOCATION, WRITE_EXTERNAL_STORAGE}, 666);
        }
*/

       // discoveredDevices.clear();
        //listAdapter.notifyDataSetChanged();

        if (discoverDevicesReceiver == null) {
            discoverDevicesReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();

                    if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                       /* if (!discoveredDevices.contains(device)) {
                            discoveredDevices.add(device);
                           // listAdapter.notifyDataSetChanged();
                        }*/
                       if (String.valueOf(device.getName()).equals("S4505M")){
                            mDevices.setText(String.valueOf(device.getAddress()));
                            address=String.valueOf(device.getAddress());
                            Connection ();
                           progressDialog.dismiss();
                        }
                    }
                }


            };
        }
        if (discoveryFinishedReceiver == null) {
            discoveryFinishedReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (progressDialog != null)
                        progressDialog.dismiss();
                    Toast.makeText(getContext(), "Устройства не найдено", Toast.LENGTH_LONG).show();
                    getActivity().unregisterReceiver(discoveryFinishedReceiver);
                }
            };
        }

        getActivity().registerReceiver(discoverDevicesReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        getActivity().registerReceiver(discoveryFinishedReceiver, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));
        progressDialog = ProgressDialog.show(getActivity(), "Поиск устройств", "Подождите...");
        bluetoothAdapter.startDiscovery();

    }
    public void Connection (){
        Log.d(TAG, "...onResume - попытка соединения...");
        Toast.makeText(getContext(),"Соединение",Toast.LENGTH_SHORT).show();
        // Set up a pointer to the remote node using it's address.
        BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);

        // Two things are needed to make a connection:
        //   A MAC address, which we got above.
        //   A Service ID or UUID.  In this case we are using the
        //     UUID for SPP.
        try {
            btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {
            Toast.makeText(getContext(),"Ошибка!",Toast.LENGTH_SHORT).show();
///////////
        }

        // Discovery is resource intensive.  Make sure it isn't going on
        // when you attempt to connect and pass your message.
        bluetoothAdapter.cancelDiscovery();

        // Establish the connection.  This will block until it connects.
        Log.d(TAG, "...Соединяемся...");
        try {
            Toast.makeText(getContext(),"Соединение устанавливается",Toast.LENGTH_SHORT).show();
            btSocket.connect();
            Toast.makeText(getContext(),"Соединение установлено и готово к передачи данных",Toast.LENGTH_SHORT).show();
            Log.d(TAG, "...Соединение установлено и готово к передачи данных...");
        } catch (IOException e) {
            try {

                btSocket.close();
                Toast.makeText(getContext(),"Ошибка!",Toast.LENGTH_SHORT).show();
                Log.d(TAG, "...Ошибка...");
            } catch (IOException e2) {
                Toast.makeText(getContext(),"Ошибка!",Toast.LENGTH_SHORT).show();
            }
        }

        // Create a data stream so we can talk to server.
        Log.d(TAG, "...Создание Socket...");
        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }
        public void run() {
            byte[] buffer = new byte[256];  // buffer store for the stream
            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);        // Получаем кол-во байт и само собщение в байтовый массив "buffer"
                    h.obtainMessage(RECIEVE_MESSAGE, bytes, -1, buffer).sendToTarget();     // Отправляем в очередь сообщений Handler
                } catch (IOException e) {
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(String message) {
            Log.d(TAG, "...Данные для отправки: " + message + "...");
            byte[] msgBuffer = message.getBytes();
            try {
                mmOutStream.write(msgBuffer);
            } catch (IOException e) {
                Log.d(TAG, "...Ошибка отправки данных: " + e.getMessage() + "...");
            }
        }
        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {

            }
        }
    }
}//2232
