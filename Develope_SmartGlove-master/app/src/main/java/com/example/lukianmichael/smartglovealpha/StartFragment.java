package com.example.lukianmichael.WatchAndFit;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.SystemClock;
import android.widget.Chronometer;
import android.widget.TextView;

import com.example.lukianmichael.WatchAndFit.data.InformationDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;

import java.util.Random;

public class StartFragment extends Activity {
    String help [] = new String[5];
    int r;
    private InformationDbHelper mDbHelper1;
    private Chronometer mChronometer;
    private TextView mTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mDbHelper1 = new InformationDbHelper(StartFragment.this);
        help[0] = "Подсказка: при создании записи для удобства вы можете воспользоваться встроенным секундомером";
        help[1] = "Подсказка: для удобства поиска нужной записи воспользуйтесь сортировкой или поиском по дате";
        help[2] = "Подсказка: теперь вы можете уведомлять своего тренера о результате пробы, воспользовавшись функцией - отправить результат";
        help[3] ="Подсказка: с помощью кардиограммы вы можете просматривать динамику своего пульса во время тренировки";
        help[4] ="Подсказка: вы можете узнать энергозатратность своей тренировки, вычисленную на основе анализа динамики пульса";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_fragment);
        mTextView = (TextView) findViewById(R.id.helpText);
        r = new Random(System.currentTimeMillis()).nextInt(5);
        mTextView.setText(help[r]);
        mChronometer = (Chronometer) findViewById(R.id.chronometer2);
        mChronometer.setBase(SystemClock.elapsedRealtime());
        mChronometer.start();
        mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long elapsedMillis = SystemClock.elapsedRealtime()
                        - mChronometer.getBase();
                if (elapsedMillis >= 2000) {
                    //Intent intent = new Intent(StartFragment.this, HelloActivity.class);
                    //startActivity(intent);
                    displayDatabaseInfo();
                    mChronometer.stop();
                    StartFragment.this.finish();
                }
            }
        });
    }
    public void displayDatabaseInfo() {
        Cursor cursor = mDbHelper1.getReadableDatabase().query(TrainingContract.Information.TABLE_NAME1, new String[]{"_id", TrainingContract.Information.COLUMN_WEIGHT, TrainingContract.Information.COLUMN_HEIGH, TrainingContract.Information.COLUMN_AGE, TrainingContract.Information.COLUMN_GENDER}, null, null, null, null, "_id DESC");
        try {
            if (cursor.getCount() == 0) {
                Intent intent = new Intent(StartFragment.this, HelloActivity.class);
                startActivity(intent);
            }
            else {
                Intent intent = new Intent(StartFragment.this, MainActivity.class);
                startActivity(intent);

            }

        } finally {
            cursor.close();
        }
    }
}
