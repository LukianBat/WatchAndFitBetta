package com.example.lukianmichael.WatchAndFit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class HelloActivity extends AppCompatActivity {
    //BasicActivity frag1;
    private Button mButton;
    // FragmentTransaction fTrans;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello);

      /*  frag1 = new BasicActivity();
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fTrans = getFragmentManager().beginTransaction();
                fTrans.replace(R.id.container, frag1);
                fTrans.commit();
            }
        });*/
    }

    public void OnClickHello(View view) {
        Intent intent = new Intent(HelloActivity.this, MainActivity.class);
        startActivity(intent);
        HelloActivity.this.finish();
    }
}