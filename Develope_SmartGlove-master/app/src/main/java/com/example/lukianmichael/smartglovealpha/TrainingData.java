package com.example.lukianmichael.WatchAndFit;

/**
 * Created by ViP on 28.06.2017.
 */

public class TrainingData {
    int srPulse;
    int Time;
    int Energy;
    int photoId;
    String date;
    TrainingData(int srPulse, int Time, int Energy, int photoId, String date) {
        this.srPulse = srPulse;
        this.Time = Time;
        this.Energy = Energy;
        this.photoId = photoId;
        this.date = date;
    }
}
