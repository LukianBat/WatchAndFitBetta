package com.example.lukianmichael.WatchAndFit;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.lukianmichael.WatchAndFit.data.OsmDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;

import java.util.ArrayList;
import java.util.List;

public class RecycleViewActivityOsm extends AppCompatActivity {
    private OsmDbHelper mDbHelper2;
    private List<OSMData> OSMDatas;
    private RecyclerView rv;
    TextView mTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Записи ОСМ");
        setContentView(R.layout.activity_recycle_view_osm);
        mTextView = (TextView) findViewById(R.id.textView21);
        rv=(RecyclerView)findViewById(R.id.rv);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        mDbHelper2 = new OsmDbHelper(RecycleViewActivityOsm.this);
        initializeData();
        initializeAdapter();
    }

    private void initializeData(){
        OSMDatas = new ArrayList<>();
        Cursor cursor = this.mDbHelper2.getReadableDatabase().query(TrainingContract.OSM.TABLE_NAME2, new String[]{"_id", TrainingContract.OSM.COLUMN_PSIT, TrainingContract.OSM.COLUMN_PSTAND, TrainingContract.OSM.COLUMN_POINT, TrainingContract.OSM.COLUMN_ZONE, TrainingContract.OSM.COLUMN_DATE}, null, null, null, null, null);
        try {
            //   display1.setText(cursor.getCount() + " измерений\n\n");
            //    display1.append("Номер - ЧСС сидя - ЧСС стоя - Балл - Зона\n");
            int idColumnIndex = cursor.getColumnIndex("_id");
            int psitColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_PSIT);
            int pstandColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_PSTAND);
            int pointColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_POINT);
            int zoneColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_ZONE);
            int dateColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_DATE);
            if (cursor.getCount() != 0) {
                while (cursor.moveToNext()) {
                    int currentID = cursor.getInt(idColumnIndex);
                    int currentPsit = cursor.getInt(psitColumnIndex);
                    int currentPstand = cursor.getInt(pstandColumnIndex);
                    int currentPoint = cursor.getInt(pointColumnIndex);
                    int currentZone = cursor.getInt(zoneColumnIndex);
                    String currentDate = cursor.getString(dateColumnIndex);
                    if (currentZone == 1) {
                        OSMDatas.add(new OSMData(currentPsit, currentPstand, currentPoint, currentZone, R.drawable.one_green, currentDate));
                    }
                    if (currentZone == 2) {
                        OSMDatas.add(new OSMData(currentPsit, currentPstand, currentPoint, currentZone, R.drawable.two_blue, currentDate));
                    }
                    if (currentZone == 3) {
                        OSMDatas.add(new OSMData(currentPsit, currentPstand, currentPoint, currentZone, R.drawable.three_yellow, currentDate));
                    }
                    if (currentZone == 4) {
                        OSMDatas.add(new OSMData(currentPsit, currentPstand, currentPoint, currentZone, R.drawable.four_red, currentDate));
                    }
                    //          display1.append("\n" + currentID + " - " + currentPsit + " уд - " + currentPstand + " уд - " + currentPoint + " баллов - " + cursor.getInt(zoneColumnIndex) + " зона ");
                }
            }
            else if (cursor.getCount() == 0){
                mTextView.setVisibility(View.VISIBLE);
            }
        } finally {
            cursor.close();
        }
    }

    private void initializeAdapter(){
        RVAdapterOsm adapter = new RVAdapterOsm(OSMDatas);
        rv.setAdapter(adapter);
    }
}
