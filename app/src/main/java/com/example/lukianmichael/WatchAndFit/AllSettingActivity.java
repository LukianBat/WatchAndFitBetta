package com.example.lukianmichael.WatchAndFit;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lukianmichael.WatchAndFit.data.KigDbHelper;
import com.example.lukianmichael.WatchAndFit.data.OsmDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;
import com.example.lukianmichael.WatchAndFit.data.TrainingDbHelper;

public class AllSettingActivity extends AppCompatActivity {
    private OsmDbHelper mDbHelper1;
    private TrainingDbHelper mDbHelper2;
    private KigDbHelper mDbHelper3;
    FragmentTransaction fTrans;
    SettingActivity frag1;
    private TextView mTextViewDelete;
    private TextView mTextViewRate;
    private TextView mTextViewVersion;
    private TextView mTextViewEdit;
    private TextView mTextViewLogIn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_setting);
        mDbHelper1 = new OsmDbHelper(this);
        mDbHelper2 = new TrainingDbHelper(this);
        mDbHelper3 = new KigDbHelper(this);
        mTextViewDelete = (TextView) findViewById(R.id.textViewDelete);
        mTextViewLogIn = (TextView) findViewById(R.id.textViewLogIn);
        mTextViewRate = (TextView) findViewById(R.id.textViewRate);
        mTextViewVersion = (TextView) findViewById(R.id.textViewVersion);
        mTextViewEdit = (TextView) findViewById(R.id.textViewEdit);
        frag1 = new SettingActivity();
        mTextViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeAll();
                Toast.makeText(getApplicationContext(),"Все записи удалены!" +
                        "", Toast.LENGTH_SHORT).show();
            }
        });
        mTextViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*
                fTrans = getFragmentManager().beginTransaction();
                fTrans.replace(R.id.container, frag1);
                fTrans.commit();
                */
            }
        });
        mTextViewRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new
                        Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.WatchAndFit"));
                startActivity(browserIntent);
            }
        });
        mTextViewLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AllSettingActivity.this, LogInActivity.class);
                startActivity(intent);
            }
        });
    }
    public void removeAll()
    {
        // db.delete(String tableName, String whereClause, String[] whereArgs);
        // If whereClause is null, it will delete all rows.
        SQLiteDatabase db = mDbHelper1.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(TrainingContract.OSM.TABLE_NAME2, null, null);
        SQLiteDatabase db1 = mDbHelper2.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db1.delete(TrainingContract.Notes.TABLE_NAME, null, null);
        SQLiteDatabase db2 = mDbHelper3.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db2.delete(TrainingContract.KIGNotes.TABLE_NAME3, null, null);
    }
}
