package com.example.lukianmichael.WatchAndFit;

/**
 * Created by ViP on 28.06.2017.
 */

public class TrainingData {
    int srPulse;
    int Time;
    int Energy;
    String date;
    TrainingData(int srPulse, int Time, int Energy, String date) {
        this.srPulse = srPulse;
        this.Time = Time;
        this.Energy = Energy;
        this.date = date;
    }
}
