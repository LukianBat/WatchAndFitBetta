package com.example.lukianmichael.WatchAndFit;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.lukianmichael.WatchAndFit.data.InformationDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;

public class KalActivity extends AppCompatActivity {
    float BMR=0;
    float IMT;
    float hard;
    float low;
    float mBMR=0;
    private InformationDbHelper mDbHelper;
    TextView mTextView;
    TextView mTextView2;
    float med;
    float min;
    float vhard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kal);
        this.mDbHelper = new InformationDbHelper(this);
        this.mTextView = (TextView) findViewById(R.id.textView12);
        this.mTextView2 = (TextView)findViewById(R.id.textView15);
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.RadioGroup);
        displayDatabaseInfo();
        mTextView.setText("Равен: "+String.format("%.2f",med)+"Ккал");
        BMR = med;
        displayDatabaseInfo();
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButtonMin:
                        mTextView.setText("Равен: " + String.format("%.2f",min) + " Ккал");
                        BMR = min;
                        displayDatabaseInfo();
                        break;
                    case R.id.radioButtonLow:
                        mTextView.setText("Равен: " + String.format("%.2f",low) + " Ккал");
                        BMR = low;
                        displayDatabaseInfo();
                        break;
                    case R.id.radioButtonMedium:
                        mTextView.setText("Равен: " + String.format("%.2f",med) + " Ккал");
                        BMR = med;
                        displayDatabaseInfo();
                        break;
                    case R.id.radioButtonHard:
                        mTextView.setText("Равен: " + String.format("%.2f",hard) + " Ккал");
                        BMR = hard;
                        displayDatabaseInfo();
                        break;
                    case R.id.radioButtonVHard:
                        mTextView.setText("Равен: " + String.format("%.2f",vhard) + " Ккал");
                        BMR = vhard;
                        displayDatabaseInfo();
                        break;
                    default:
                        break;
                }
            }
        });
    }
    public void displayDatabaseInfo() {
        Cursor cursor = mDbHelper.getReadableDatabase().query(TrainingContract.Information.TABLE_NAME1, new String[]{"_id", TrainingContract.Information.COLUMN_WEIGHT, TrainingContract.Information.COLUMN_HEIGH, TrainingContract.Information.COLUMN_AGE, TrainingContract.Information.COLUMN_GENDER}, null, null, null, null, "_id DESC");
        try {
            int idColumnIndex = cursor.getColumnIndex("_id");
            int weightColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_WEIGHT);
            int heighColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_HEIGH);
            int ageColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_AGE);
            int genderColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_GENDER);
            while (cursor.moveToNext()) {
                int currentID = cursor.getInt(idColumnIndex);
                int currentWeight = cursor.getInt(weightColumnIndex);
                int currentHeigh = cursor.getInt(heighColumnIndex);
                int currentAge = cursor.getInt(ageColumnIndex);
                int currentGender = cursor.getInt(genderColumnIndex);
                if (currentGender==1){
                    mBMR= (float) (10*currentWeight+6.25*currentHeigh-5*currentAge+5); // Формула Сан-Жеора суточный расход энергии
                    min= (float) (mBMR*1.2);
                    low= (float) (mBMR*1.375);
                    med= (float) (mBMR*1.55);
                    hard= (float) (mBMR*1.725);
                    vhard= (float) (mBMR*1.9);
                }
                if (currentGender==2){
                    mBMR= (float) (10*currentWeight+6.25*currentHeigh-5*currentAge+161);
                    min= (float) (mBMR*1.2);
                    low= (float) (mBMR*1.375);
                    med= (float) (mBMR*1.55);
                    hard= (float) (mBMR*1.725);
                    vhard= (float) (mBMR*1.9);
                }
                IMT= (float) ((currentWeight)/(currentHeigh*currentHeigh/10000.0));
                mTextView2.setText(BMR+"");
                if (currentAge>=16 && currentAge<=25){
                    if (IMT<17.5){
                        mTextView2.setText(R.string.IMT_lean);
                    }
                    if (IMT>=17.5 && IMT<18.5){
                        mTextView2.setText(R.string.IMT_smal_lean);
                    }
                    if (IMT>=18.5 && IMT<23){
                        mTextView2.setText(R.string.IMT_norm);
                    }
                    if (IMT>=23 && IMT<27.5){
                        mTextView2.setText(R.string.IMT_small_fat);
                        if (currentWeight/0.45*8<(BMR-(BMR*0.2))) {
                            mTextView2.append(String.format("%.2f",(BMR - (BMR * 0.2))));
                        }
                    }
                    if (IMT>=27.5 && IMT<30){
                        mTextView2.setText(R.string.IMT_fat1);
                        if (currentWeight/0.45*8<(BMR-(BMR*0.2))) {
                            mTextView2.append(String.format("%.2f",(BMR - (BMR * 0.2))));
                        }
                    }
                    if (IMT>=30 && IMT<35){
                        mTextView2.setText(R.string.IMTfat2);
                        if (currentWeight/0.45*8<(BMR-(BMR*0.2))) {
                            mTextView2.append(String.format("%.2f",(BMR - (BMR * 0.2))));
                        }
                    }
                    if (IMT>=35 && IMT<40){
                        mTextView2.setText(R.string.IMTfat3);
                        if (currentWeight/0.45*8<(BMR-(BMR*0.2))) {
                            mTextView2.append(String.format("%.2f",(BMR - (BMR * 0.2))));
                        }
                    }
                    if (IMT>=40){
                        mTextView2.setText(R.string.IMTfat4);
                        if (currentWeight/0.45*8<(BMR-(BMR*0.2))) {
                            mTextView2.append(String.format("%.2f",(BMR - (BMR * 0.2))));
                        }
                    }

                }
                if (currentAge>=25){
                    if (IMT<17.5){
                        mTextView2.setText(R.string.IMT_lean);
                    }
                    if (IMT>=17.5 && IMT<18.5){
                        mTextView2.setText(R.string.IMT_smal_lean);
                    }
                    if (IMT>=20 && IMT<26){
                        mTextView2.setText(R.string.IMT_norm);
                    }
                    if (IMT>=26 && IMT<28){
                        mTextView2.setText(R.string.IMT_small_fat);
                    }
                    if (IMT>=28 && IMT<31){
                        mTextView2.setText(R.string.IMT_fat1);
                        if (currentWeight/0.45*8<(BMR-(BMR*0.2))) {
                            mTextView2.append(String.format("%.2f",(BMR - (BMR * 0.2))));
                        }
                    }
                    if (IMT>=31 && IMT<36){
                        mTextView2.setText(R.string.IMTfat2);
                        if (currentWeight/0.45*8<(BMR-(BMR*0.2))) {
                            mTextView2.append(String.format("%.2f",(BMR - (BMR * 0.2))));
                        }
                    }
                    if (IMT>=36 && IMT<41){
                        mTextView2.setText(R.string.IMTfat3);
                        if (currentWeight/0.45*8<(BMR-(BMR*0.2))) {
                            mTextView2.append(String.format("%.2f",(BMR - (BMR * 0.2))));
                        }
                    }
                    if (IMT>=41){
                        mTextView2.setText(R.string.IMTfat4);
                        if (currentWeight/0.45*8<(BMR-(BMR*0.2))) {
                            mTextView2.append(String.format("%.2f",(BMR - (BMR * 0.2))));
                        }
                    }

                }
            }
        } finally {
            cursor.close();
        }
    }

}