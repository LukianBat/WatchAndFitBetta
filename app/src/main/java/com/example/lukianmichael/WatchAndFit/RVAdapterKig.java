package com.example.lukianmichael.WatchAndFit;

/**
 * Created by ViP on 28.06.2017.
 */

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class RVAdapterKig extends RecyclerView.Adapter<RVAdapterKig.PersonViewHolder> {

    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView indexpower;
        TextView dx;
        TextView amo;
        TextView mo;
        TextView mdate;
        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv3);
            indexpower = (TextView)itemView.findViewById(R.id.indexpower);
            dx = (TextView)itemView.findViewById(R.id.dx);
            amo = (TextView)itemView.findViewById(R.id.amo);
            mo = (TextView)itemView.findViewById(R.id.mo);
            mdate = (TextView) itemView.findViewById(R.id.textViewDate3);
        }
    }

    List<KigData> KigDatas;
    RVAdapterKig(List<KigData> KigDatas){
        this.KigDatas = KigDatas;
    }
    @Override
    public int getItemCount() {
        return KigDatas.size();
    }
    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_kig, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }
    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.indexpower.append(" "+KigDatas.get(i).Indexpower);
        personViewHolder.dx.append(" "+KigDatas.get(i).Dx);
        personViewHolder.amo.append(" "+ KigDatas.get(i).Amo);
        personViewHolder.mo.append(" "+ KigDatas.get(i).Mo);
        personViewHolder.mdate.append(" "+KigDatas.get(i).date);
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}