package com.example.lukianmichael.WatchAndFit;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lukianmichael.WatchAndFit.data.InformationDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class StartFragment extends Activity {
    private InformationDbHelper mDbHelper1;
    private Chronometer mChronometer;
    StorageReference riversRef;
    String Name;
    FirebaseUser user;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private StorageReference mStorageRef;
    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Name = user.getEmail();
                    mStorageRef = FirebaseStorage.getInstance().getReference();
                    riversRef = mStorageRef.child(Name + "/inf.db");
                    try {
                        download();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        };
        mDbHelper1 = new InformationDbHelper(StartFragment.this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_fragment);
        mChronometer = (Chronometer) findViewById(R.id.chronometer2);
        mChronometer.setBase(SystemClock.elapsedRealtime());
        mChronometer.start();
        mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long elapsedMillis = SystemClock.elapsedRealtime()
                        - mChronometer.getBase();
                if (elapsedMillis >= 1500) {
                    displayDatabaseInfo();
                    mChronometer.stop();
                    StartFragment.this.finish();
                }
            }
        });
    }
    public void displayDatabaseInfo() {
        Cursor cursor = mDbHelper1.getReadableDatabase().query(TrainingContract.Information.TABLE_NAME1, new String[]{"_id", TrainingContract.Information.COLUMN_WEIGHT, TrainingContract.Information.COLUMN_HEIGH, TrainingContract.Information.COLUMN_AGE, TrainingContract.Information.COLUMN_GENDER}, null, null, null, null, "_id DESC");
        try {
            if (cursor.getCount() == 0) {
                Intent intent = new Intent(StartFragment.this, HelloActivity.class);
                startActivity(intent);
            }
            else {
                Intent intent = new Intent(StartFragment.this, MainActivity.class);
                startActivity(intent);

            }

        } finally {
            cursor.close();
        }
    }
    public void download() throws IOException {
        final File localFile = new File(Environment.getDataDirectory(), "//data//" + "com.example.lukianmichael.WatchAndFit"
                + "//databases//" + "inf.db");
        riversRef.getFile(localFile)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
            }
        });

    }
}
