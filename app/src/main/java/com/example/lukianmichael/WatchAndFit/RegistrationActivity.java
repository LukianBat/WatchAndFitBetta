package com.example.lukianmichael.WatchAndFit;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class RegistrationActivity extends Activity implements View.OnClickListener {
    String action = "auth";
    FirebaseUser user;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private EditText Email;
    private EditText Password;
    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    String email = user.getEmail();
                    Intent i = new Intent(RegistrationActivity.this, SucEnter.class);
                    i.putExtra("name", email);
                    i.putExtra("action", action);
                    startActivity(i);
                    // User is signed in
                } else {
                    // User is signed out
                }
                // ...
            }
        };
        Email = (EditText) findViewById(R.id.et_email);
        Password = (EditText) findViewById(R.id.et_password);
        //PasswordRepeat = (EditText) findViewById(R.id.rep_pass);
        findViewById(R.id.btn_sign_in).setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_sign_in && Email.toString() != null && Password.toString() != null) {
            Registration(Email.getText().toString(), Password.getText().toString());
        }
    }
    public void Registration(final String email, String password) {
        action = "reg";
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    user.sendEmailVerification();
                    Toast.makeText(RegistrationActivity.this, "Регистрация прошла успешно!\n" +
                            "Письмо с подтверждением отправлено вам на почту", Toast.LENGTH_SHORT).show();
//
                } else {
                    Toast.makeText(RegistrationActivity.this, "Такой пользователь уже существует!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}