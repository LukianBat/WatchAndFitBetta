package com.example.lukianmichael.WatchAndFit;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.lukianmichael.WatchAndFit.data.KigDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;

public class ResultsKIG extends Fragment {
    private KigDbHelper mDbHelper;
    private Button mButton;
    private String dateNote;
    private TextView mTextViewResults;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item_osm clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.share) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_TEXT, dateNote);
            i.putExtra(Intent.EXTRA_SUBJECT, "Отправка результатов КИГ");
            i = Intent.createChooser(i, "Отправить с помощью");
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.activity_results_kig, container, false);
        mDbHelper = new KigDbHelper(getActivity());
        mButton = (Button) rootView.findViewById(R.id.button11);
        mTextViewResults = (TextView) rootView.findViewById(R.id.textView31);
        displayDatabaseInfo();
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
                Intent intent = new Intent(getActivity(),MainActivity.class);
                startActivity(intent);
            }
        });
        return rootView;
    }
    public void displayDatabaseInfo() {
        Cursor cursor = this.mDbHelper.getReadableDatabase().query(TrainingContract.KIGNotes.TABLE_NAME3, new String[]{"_id",
                        TrainingContract.KIGNotes.COLUMN_AMO,
                        TrainingContract.KIGNotes.COLUMN_DATE,
                        TrainingContract.KIGNotes.COLUMN_DX,
                        TrainingContract.KIGNotes.COLUMN_MO,
                        TrainingContract.KIGNotes.COLUMN_INDEXPOWER,},
                null,
                null,
                null,
                null,
                "_id DESC");
        try {
            int idColumnIndex = cursor.getColumnIndex("_id");
            int dateColumnIndex = cursor.getColumnIndex(TrainingContract.KIGNotes.COLUMN_DATE);
            int indexpowerColumnIndex = cursor.getColumnIndex(TrainingContract.KIGNotes.COLUMN_INDEXPOWER);
            int dxColumnIndex = cursor.getColumnIndex(TrainingContract.KIGNotes.COLUMN_DX);
            int amoColumnIndex = cursor.getColumnIndex(TrainingContract.KIGNotes.COLUMN_AMO);
            int moColumnIndex = cursor.getColumnIndex(TrainingContract.KIGNotes.COLUMN_MO);
            if (cursor.moveToNext()) {
                int currentID = cursor.getInt(idColumnIndex);
                String datecurrent = cursor.getString(dateColumnIndex);
                int currentindexpower = cursor.getInt(indexpowerColumnIndex);
                int currentdx = cursor.getInt(dxColumnIndex);
                int currentamo = cursor.getInt(amoColumnIndex);
                int currentmo = cursor.getInt(moColumnIndex);
                mTextViewResults.setText("Индекс напряжённости: "+currentindexpower+"\n"+
                        "Вариационный размах: "+"\n"+currentdx/10.0+"сек\n"+"Амплитуда моды: "+currentamo+"\n"+"Мода: "+currentmo/10.0+"\n"+"Дата: "+
                        datecurrent);
                dateNote=("Результаты КАРДИОИНТЕРВАЛОГРАФИИ"+"Индекс напряжённости: "+currentindexpower+"\n"+
                        "Вариационный размах: "+"\n"+currentdx/10.0+"сек\n"+"Амплитуда моды: "+currentamo+"\n"+"Мода: "+currentmo/10.0+"\n"+"Дата: "+datecurrent);
            }
            cursor.close();
        } catch (Throwable th) {
            cursor.close();
        }
    }
}
/*
Cursor cursor = mDbHelper.getReadableDatabase().query(TrainingContract.KIGNotes.TABLE_NAME3, new String[]{"_id",
                        TrainingContract.KIGNotes.COLUMN_DATE,
                TrainingContract.KIGNotes.COLUMN_INDEXPOWER,
                TrainingContract.KIGNotes.COLUMN_DX,
                TrainingContract.KIGNotes.COLUMN_MO,
                null, null, null, null, "_id DESC");
        try {
            int idColumnIndex = cursor.getColumnIndex("_id");
            int dateColumnIndex = cursor.getColumnIndex(TrainingContract.KIGNotes.COLUMN_DATE);
            int indexpowerColumnIndex = cursor.getColumnIndex(TrainingContract.KIGNotes.COLUMN_INDEXPOWER);
            int dxColumnIndex = cursor.getColumnIndex(TrainingContract.KIGNotes.COLUMN_DX);
            int amoColumnIndex = cursor.getColumnIndex(TrainingContract.KIGNotes.COLUMN_AMO);
            int moColumnIndex = cursor.getColumnIndex(TrainingContract.KIGNotes.COLUMN_MO);
            while (cursor.moveToNext()) {
                int currentID = cursor.getInt(idColumnIndex);
                String datecurrent = cursor.getString(dateColumnIndex);
                float currentindexpower = cursor.getInt(indexpowerColumnIndex);
                int currentdx = cursor.getInt(dxColumnIndex);
                float currentamo = cursor.getInt(amoColumnIndex);
                int currentmo = cursor.getInt(moColumnIndex);
                mTextViewResults.setText("Индекс напряжённости: "+currentindexpower+"\n"+
                        "Вариационный размах: "+"\n"+currentdx+"сек\n"+"Амплитуда моды: "+currentamo+"\n"+"Мода: "+currentmo);
                cursor.close();
            }
        } finally {
            cursor.close();
        }
 */
