package com.example.lukianmichael.WatchAndFit;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.lukianmichael.WatchAndFit.data.InformationDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;
import com.example.lukianmichael.WatchAndFit.data.TrainingDbHelper;

public class MainActivity extends AppCompatActivity {
    private LinearLayout mLinearLayout;
    private TrainingDbHelper mDbHelper;
    private InformationDbHelper mDbHelper1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDbHelper1 = new InformationDbHelper(MainActivity.this);
        displayDatabaseInfo();
        mDbHelper = new TrainingDbHelper(MainActivity.this);
    }

/*
 Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, getNoteReport());
                i.putExtra(Intent.EXTRA_SUBJECT, "Отправка результатов");
                i = Intent.createChooser(i, "Отправить с помощью");
                startActivity(i);
*/
/*
        mLinearLayout = (LinearLayout) findViewById(R.id.LinearLayoutHeader);
        mLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = null;
                Class fragmentClass = null;
                fragmentClass = BasicActivity.class;
                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                setTitle("Профиль пользователя");
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

*/
/*

        if (id == R.id.nav_OCM) {
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = AddActivity.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setTitle("ОСМ");
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            // Handle the camera action
        } else if (id == R.id.nav_graph) {
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = GraphicActivity.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setTitle("Тренировка");
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();

        } else if (id == R.id.nav_setting) {
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = AllSettingActivity.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            setTitle("Настройки");
        } else if (id == R.id.nav_kal) {
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = KalActivity.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setTitle("Расчёт каллорий");
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();

        }else if (id == R.id.nav_info) {
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = InfoActivity.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setTitle("Инструкция");
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();

        }else if (id == R.id.nav_kardio) {
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = KardioIntervalActivity.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setTitle("Кардиоинтервалография");
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();

        }
        else if (id == R.id.nav_share) {
            String date="0";
            int countDay = 0;
            Cursor cursor = this.mDbHelper.getReadableDatabase().query(TrainingContract.Notes.TABLE_NAME, new String[]{"_id",  TrainingContract.OSM.COLUMN_DATE}, null, null, null, null, null);
            try {
                int idColumnIndex = cursor.getColumnIndex("_id");
                int dateColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_DATE);
                if (cursor.getCount() != 0) {
                    while (cursor.moveToNext()) {
                        String currentDate = cursor.getString(dateColumnIndex);
                        if (!currentDate.equals(date)){
                            countDay++;
                            date=currentDate;
                        }
                    }
                }
                else if (cursor.getCount() == 0){
                    Toast.makeText(getApplicationContext(),"Вы еще не сделали ни одной записи тренировок", Toast.LENGTH_SHORT).show();
                }
            } finally {
                cursor.close();
            }
            if (countDay!=0){
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, "Я тренируюсь вместе FingerPulse уже "+countDay+" дней! \n Присоединяйтесь! https://play.google.com/store/apps/details?id=com.smartglovealpha");
                i.putExtra(Intent.EXTRA_SUBJECT, "Отправка результатов");
                i = Intent.createChooser(i, "Поделиться с помощью...");
                startActivity(i);
            }
        }

*/
   /* public void OnClickHead(View view) {
        Fragment fragment = null;
        Class fragmentClass = null;
        fragmentClass = BasicActivity.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
    }
    */

    public void displayDatabaseInfo() {
        Cursor cursor = mDbHelper1.getReadableDatabase().query(TrainingContract.Information.TABLE_NAME1, new String[]{"_id", TrainingContract.Information.COLUMN_WEIGHT, TrainingContract.Information.COLUMN_HEIGH, TrainingContract.Information.COLUMN_AGE, TrainingContract.Information.COLUMN_GENDER}, null, null, null, null, "_id DESC");
        try {
            if (cursor.getCount() == 0) {
                Fragment fragment = null;
                Class fragmentClass = null;
                fragmentClass = SettingActivity.class;
                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                setTitle("Настройки");
                /*
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                */
            }
            else {
                Fragment fragment = null;
                Class fragmentClass = null;
                setTitle("Профиль пользователя");
                fragmentClass = BasicActivity.class;
                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                */
            }
        } finally {
            cursor.close();
        }
    }

    public void OnClickBasic(View view) {
        Intent intent = new Intent(MainActivity.this, BasicActivity.class);
        startActivity(intent);
    }

    public void OnClickOsm(View view) {
        Intent intent = new Intent (MainActivity.this, AddActivity.class);
        startActivity(intent);
    }
    public void OnClickGraphic(View view){
        Intent intent = new Intent(MainActivity.this, GraphicActivity.class);
        startActivity(intent);
    }
    public  void OnClickSettings(View view){
        Intent intent = new Intent(MainActivity.this, AllSettingActivity.class);
        startActivity(intent);
    }

    public void OnClickKal(View view) {
        Intent intent = new Intent(MainActivity.this, KalActivity.class);
        startActivity(intent);
    }

    public void OnClickKig(View view) {
        Toast.makeText(getApplicationContext(),"Ожидается в будущих обновлениях", Toast.LENGTH_SHORT).show();
    }

    public void OnClickInfo(View view) {
        Intent intent = new Intent(MainActivity.this, InfoActivity.class);
        startActivity(intent);
    }
}
