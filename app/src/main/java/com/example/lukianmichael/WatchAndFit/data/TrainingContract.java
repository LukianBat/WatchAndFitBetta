package com.example.lukianmichael.WatchAndFit.data;

import android.provider.BaseColumns;

public final class TrainingContract {

    public static final class Information implements BaseColumns {
        public static final String COLUMN_AGE = "age";
        public static final String COLUMN_GENDER = "gender";
        public static final String COLUMN_HEIGH = "heigh";
        public static final String COLUMN_WEIGHT = "weight";
        public static final String TABLE_NAME1 = "inf";
        public static final String _ID = "_id";
    }

    public static final class Notes implements BaseColumns {
        public static final String COLUMN_ENERGY = "energy";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_PULSE = "pulse";
        public static final String COLUMN_TIME = "time";
        public static final String TABLE_NAME = "training";
        public static final String _ID = "_id";
    }

    public static final class OSM implements BaseColumns {
        public static final String COLUMN_POINT = "point";
        public static final String COLUMN_PSIT = "psit";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_PSTAND = "pstand";
        public static final String COLUMN_ZONE = "zone";
        public static final String TABLE_NAME2 = "osm";
        public static final String _ID = "_id";
    }
    public static final class KIGNotes implements BaseColumns {
        public static final String _ID = "_id";
        public static final String COLUMN_AMO = "amo";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_DX = "dx";
        public static final String COLUMN_MO = "mo";
        public static final String COLUMN_INDEXPOWER = "indexpower";
        public static final String TABLE_NAME3 = "kig";

    }
    public static final class NeuroOSM implements BaseColumns {
        public static final String _ID = "_id";
        public static final String COLUMN_W = "W";
        public static final String TABLE_NAME4 = "neuroosm";
    }
    public static final class NeuroKIG implements BaseColumns {
        public static final String _ID = "_id";
        public static final String COLUMN_W = "W";
        public static final String TABLE_NAME5 = "neurokig";
    }
}
