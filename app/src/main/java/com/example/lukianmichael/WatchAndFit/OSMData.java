package com.example.lukianmichael.WatchAndFit;

/**
 * Created by ViP on 28.06.2017.
 */

class OSMData {
    int Psit;
    int Pstand;
    int Point;
    int Zone;
    int photoId;
    String date;

    OSMData(int Psit, int Pstand, int Point, int Zone, int photoId, String date) {
        this.Psit = Psit;
        this.Pstand = Pstand;
        this.Point = Point;
        this.Zone = Zone;
        this.photoId = photoId;
        this.date = date;
    }

    // This method creates an ArrayList that has three OSMData objects
// Checkout the project associated with this tutorial on Github if
// you want to use the same images.

}