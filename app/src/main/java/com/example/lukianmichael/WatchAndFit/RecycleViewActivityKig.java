package com.example.lukianmichael.WatchAndFit;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.lukianmichael.WatchAndFit.data.KigDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;

import java.util.ArrayList;
import java.util.List;

public class RecycleViewActivityKig extends AppCompatActivity {
    private KigDbHelper mDbHelper2;
    private List<KigData> KigDatas;
    private RecyclerView rv;
    TextView mTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Записи КИГ");
        setContentView(R.layout.activity_recycle_view_kig);
        mTextView = (TextView) findViewById(R.id.textView210);
        rv=(RecyclerView)findViewById(R.id.rv3);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        mDbHelper2 = new KigDbHelper(RecycleViewActivityKig.this);
        initializeData();
        initializeAdapter();
    }

    private void initializeData(){
        KigDatas = new ArrayList<>();
        Cursor cursor = this.mDbHelper2.getReadableDatabase().query(TrainingContract.KIGNotes.TABLE_NAME3, new String[]{"_id",
                        TrainingContract.KIGNotes.COLUMN_AMO,
                        TrainingContract.KIGNotes.COLUMN_DATE,
                        TrainingContract.KIGNotes.COLUMN_DX,
                        TrainingContract.KIGNotes.COLUMN_MO,
                        TrainingContract.KIGNotes.COLUMN_INDEXPOWER,},
                null,
                null,
                null,
                null,
                "_id DESC");        try {
            //   display1.setText(cursor.getCount() + " измерений\n\n");
            //    display1.append("Номер - ЧСС сидя - ЧСС стоя - Балл - Зона\n");
            int idColumnIndex = cursor.getColumnIndex("_id");
            int dateColumnIndex = cursor.getColumnIndex(TrainingContract.KIGNotes.COLUMN_DATE);
            int indexpowerColumnIndex = cursor.getColumnIndex(TrainingContract.KIGNotes.COLUMN_INDEXPOWER);
            int dxColumnIndex = cursor.getColumnIndex(TrainingContract.KIGNotes.COLUMN_DX);
            int amoColumnIndex = cursor.getColumnIndex(TrainingContract.KIGNotes.COLUMN_AMO);
            int moColumnIndex = cursor.getColumnIndex(TrainingContract.KIGNotes.COLUMN_MO);
            if (cursor.getCount() != 0) {
                while (cursor.moveToNext()) {
                    int currentID = cursor.getInt(idColumnIndex);
                    String datecurrent = cursor.getString(dateColumnIndex);
                    double currentindexpower = cursor.getInt(indexpowerColumnIndex);
                    double currentdx = cursor.getInt(dxColumnIndex);
                    double currentamo = cursor.getInt(amoColumnIndex);
                    double currentmo = cursor.getInt(moColumnIndex);
                    KigDatas.add(new KigData(currentdx/10.0, currentamo, currentmo/10.0, currentindexpower, datecurrent));
                    /*
                    if (currentZone == 1) {
                        OSMDatas.add(new OSMData(currentPsit, currentPstand, currentPoint, currentZone, R.drawable.one_green, currentDate));
                    }
                    if (currentZone == 2) {
                        OSMDatas.add(new OSMData(currentPsit, currentPstand, currentPoint, currentZone, R.drawable.two_blue, currentDate));
                    }
                    if (currentZone == 3) {
                        OSMDatas.add(new OSMData(currentPsit, currentPstand, currentPoint, currentZone, R.drawable.three_yellow, currentDate));
                    }
                    if (currentZone == 4) {
                        OSMDatas.add(new OSMData(currentPsit, currentPstand, currentPoint, currentZone, R.drawable.four_red, currentDate));
                    }
                    */
                    //          display1.append("\n" + currentID + " - " + currentPsit + " уд - " + currentPstand + " уд - " + currentPoint + " баллов - " + cursor.getInt(zoneColumnIndex) + " зона ");
                }
            }
            else if (cursor.getCount() == 0){
                mTextView.setVisibility(View.VISIBLE);
            }
        } finally {
            cursor.close();
        }
    }

    private void initializeAdapter(){
        RVAdapterKig adapter = new RVAdapterKig(KigDatas);
        rv.setAdapter(adapter);
    }
}
