package com.example.lukianmichael.WatchAndFit;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lukianmichael.WatchAndFit.data.InformationDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;
import com.example.lukianmichael.WatchAndFit.data.TrainingDbHelper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.File;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;


public class GraphicActivity extends AppCompatActivity implements SensorEventListener {
    int heartrate;
    private Timer mTimer;
    private MyTimerTask mMyTimerTask;
    double srPulse = 0;
    String date;
    long Time = 0;
    long TimeError=0;
    TextView mTextView;
    private Chronometer mChronometer;
    long TimePause = 0;
    private ImageButton mButton;
    private ImageButton mButton2;
    private TrainingDbHelper mDbHelper;
    private InformationDbHelper mDbHelper1;
    int x = 0;
    int y;
    int x0 = 0;
    int y0 = 0;
    int weight;
    GraphView graph;
    Sensor mHeartRateSensor;
    SensorManager mSensorManager;
    String Name;
    StorageReference riversRef3;
    private StorageReference mStorageRef;
    FirebaseUser user;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graphic);
        graph = (GraphView)findViewById(R.id.graph);
        mSensorManager = ((SensorManager)getSystemService(SENSOR_SERVICE));
        mHeartRateSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
        mTextView = (TextView) findViewById(R.id.textView20);
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(0, 0)
        });
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(250);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(10);

        graph.getViewport().setScalable(true);
        graph.getViewport().setScalableY(false);
        graph.addSeries(series);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Name = user.getEmail();
                    Toast.makeText(GraphicActivity.this,Name,Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(GraphicActivity.this, "Нет соединения с интернетом, или вы не авторизированы!", Toast.LENGTH_SHORT).show();
                }
            }
        };

        mChronometer = (Chronometer)findViewById(R.id.chronometer);
        mButton = (ImageButton) findViewById(R.id.button2);
        mButton2 = (ImageButton)findViewById(R.id.button4);
        mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long elapsedMillis = SystemClock.elapsedRealtime()
                        - mChronometer.getBase();
                if (heartrate!=0) {
                    Time++;
                    mTextView.setText("Время: " + String.format("%.2f", ((Time)/60.0)) + " мин Пульс: " + y);
                    srPulse = srPulse + y;
                    Log.i("LOG", "Pulse="+y+"  PulseSumm="+srPulse+" Time(sec)="+Time);
                } else{
                    mTextView.setText("Идёт настройка приёма данных!");
                }
            }
        });
        mDbHelper1 = new InformationDbHelper(this);
        mDbHelper = new TrainingDbHelper(this);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        mButton.setClickable(false);
                        if (mTimer != null) {
                            mTimer.cancel();
                        }
                        mTimer = new Timer();
                        mMyTimerTask = new MyTimerTask();
                        mTimer.schedule(mMyTimerTask, 0, 200);
                        mChronometer.setBase(SystemClock.elapsedRealtime());
                        mChronometer.start();
                        mButton.setVisibility(View.GONE);
                        mButton2.setVisibility(View.VISIBLE);
            }
        });
        mButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    mChronometer.stop();
                    srPulse /= Time;
                    insertTraining();
                    if (mTimer != null) {
                        mTimer.cancel();
                        mTimer = null;
                    }
                    mStorageRef = FirebaseStorage.getInstance().getReference();
                    riversRef3 = mStorageRef.child(Name + "/training.db");
                    upload();
                    Intent intent = new Intent(GraphicActivity.this, ResultsActivity.class);
                    startActivity(intent);
                    finish();

            }
        });

        Calendar c = Calendar.getInstance();
        if ((c.get(c.MONTH)+1)>=10){
            date = String.valueOf(c.get(c.DAY_OF_MONTH)) + ":" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
        }else {
            if ((c.get(c.MONTH) + 1) < 10) {
                date = String.valueOf(c.get(c.DAY_OF_MONTH)) + ":0" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
            }
            if (c.get(c.DAY_OF_MONTH) < 10) {
                date = "0" + String.valueOf(c.get(c.DAY_OF_MONTH)) + ":" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
            }
            if ((c.get(c.MONTH) + 1) < 10) {
                date = String.valueOf(c.get(c.DAY_OF_MONTH)) + ":0" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
            }
            if ((c.get(c.DAY_OF_MONTH) < 10) && ((c.get(c.MONTH) + 1) < 10)) {
                date = "0" + String.valueOf(c.get(c.DAY_OF_MONTH)) + ":0" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (mSensorManager != null){
            mSensorManager.registerListener(this, mHeartRateSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mSensorManager!=null)
            mSensorManager.unregisterListener(this);
    }
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_HEART_RATE) {
            heartrate = (int) event.values[0];
            y = heartrate;
        }
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
    public void sendWeight() {
        Cursor cursor = this.mDbHelper1.getReadableDatabase().query(TrainingContract.Information.TABLE_NAME1, new String[]{"_id", TrainingContract.Information.COLUMN_WEIGHT, TrainingContract.Information.COLUMN_HEIGH, TrainingContract.Information.COLUMN_AGE}, null, null, null, null, null);
        try {
            int weightColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_WEIGHT);
            if (cursor.moveToNext()) {
                weight = cursor.getInt(weightColumnIndex);
            }
            cursor.close();
        } catch (Throwable th) {
            cursor.close();
        }
    }
    private void insertTraining() {
        sendWeight();
        SQLiteDatabase db = this.mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TrainingContract.Notes.COLUMN_PULSE, Double.valueOf(srPulse));
        values.put(TrainingContract.Notes.COLUMN_TIME, Float.valueOf(Time));
        values.put(TrainingContract.Notes.COLUMN_ENERGY, Double.valueOf(1000.0 * (((0.014 * (weight)) * (Time / 60.0)) * ((0.12 * srPulse) - 7.0))));
        values.put(TrainingContract.Notes.COLUMN_DATE, String.valueOf(date));
        long newRowId = db.insert(TrainingContract.Notes.TABLE_NAME, null, values);
    }
    class MyTimerTask extends TimerTask {

        @Override
        public void run() {

            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    long elapsedMillis = SystemClock.elapsedRealtime()
                            - mChronometer.getBase();
                    LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[]{
                            new DataPoint(x0,y0),
                            new DataPoint(x,y)
                    });

                    graph.getViewport().setYAxisBoundsManual(true);
                    graph.getViewport().setMinY(0);
                    graph.getViewport().setMaxY(250);
                    graph.getViewport().setXAxisBoundsManual(true);
                    graph.getViewport().setMinX(0);
                    graph.getViewport().setMaxX(10);
                    graph.getViewport().setScalable(true);
                    graph.getViewport().setScalableY(false);
                    graph.addSeries(series);
                    graph.getViewport().setXAxisBoundsManual(true);
                    if (x<5){
                        graph.getViewport().setMinX(0);
                        graph.getViewport().setMaxX(x+5);
                    }
                    else {
                        graph.getViewport().setMinX(x - 5);
                        graph.getViewport().setMaxX(x + 5);
                    }
                    x0=x;
                    y0=y;
                    x++;
                }
            });
        }
    }
    public void upload() {

        final Uri file3 = Uri.fromFile(new File(Environment.getDataDirectory(), "//data//" + "com.example.lukianmichael.WatchAndFit"
                + "//databases//" + "training.db"));
        Toast.makeText(GraphicActivity.this, "Идет загрузка", Toast.LENGTH_SHORT).show();

        riversRef3.putFile(file3)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        Toast.makeText(GraphicActivity.this, "Загрузка завершена успешно!", Toast.LENGTH_SHORT).show();
                        //Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Toast.makeText(GraphicActivity.this, "Ошибка!", Toast.LENGTH_SHORT).show();
                        // ...
                    }
                });

    }

}