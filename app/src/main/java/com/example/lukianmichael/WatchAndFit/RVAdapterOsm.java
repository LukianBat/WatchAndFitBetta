package com.example.lukianmichael.WatchAndFit;

/**
 * Created by ViP on 28.06.2017.
 */

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class RVAdapterOsm extends RecyclerView.Adapter<RVAdapterOsm.PersonViewHolder> {

    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView personPoint;
        TextView personPstand;
        TextView personPsit;
        TextView personZone;
        ImageView personPhoto;
        TextView mdate;

        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);

            personPoint = (TextView)itemView.findViewById(R.id.point);
            personZone = (TextView)itemView.findViewById(R.id.zone);
            personPsit = (TextView)itemView.findViewById(R.id.psit);
            personPstand = (TextView)itemView.findViewById(R.id.pstand);
            personPhoto = (ImageView)itemView.findViewById(R.id.person_photo);
            mdate = (TextView) itemView.findViewById(R.id.textViewDate);
        }
    }

    List<OSMData> OSMDatas;

    RVAdapterOsm(List<OSMData> OSMDatas){
        this.OSMDatas = OSMDatas;
    }
    @Override
    public int getItemCount() {
        return OSMDatas.size();
    }
    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_osm, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }
    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.personPoint.setText("Балл: "+ OSMDatas.get(i).Point);
        personViewHolder.personPstand.setText("Пульс стоя: "+ OSMDatas.get(i).Pstand);
        personViewHolder.personPsit.setText("Пульс сидя: "+ OSMDatas.get(i).Psit);
        personViewHolder.personZone.setText("Зона: "+ OSMDatas.get(i).Zone);
        personViewHolder.personPhoto.setImageResource(OSMDatas.get(i).photoId);
        personViewHolder.mdate.setText("Дата: "+OSMDatas.get(i).date);
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}