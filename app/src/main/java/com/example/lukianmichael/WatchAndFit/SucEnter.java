package com.example.lukianmichael.WatchAndFit;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.lukianmichael.WatchAndFit.data.InformationDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import java.io.File;
import java.io.IOException;
public class SucEnter extends AppCompatActivity {
    StorageReference riversRef;
    StorageReference riversRef1;
    StorageReference riversRef2;
    StorageReference riversRef3;
    StorageReference riversRef4;
    private InformationDbHelper mDbHelper1;

    String Name;
    String Action;
    FirebaseUser user;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private RadioButton R1;
    private RadioButton R2;
    private RadioButton R3;
    private StorageReference mStorageRef;
    private Button mButton;
    private Button mButtonOut;

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(SucEnter.this, MainActivity.class);
        startActivity(intent);
        super.onBackPressed();  // optional depending on your needs
    }
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDbHelper1 = new InformationDbHelper(SucEnter.this);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

            }
        };
        final Intent intent = getIntent();
        Name = intent.getStringExtra("name");
        Action = intent.getStringExtra("action");
        Log.i("LOG", Action);

        if (Action.equals("reg")) {
            setContentView(R.layout.succes_reg);


        } else
            setContentView(R.layout.succes_enter);

        mStorageRef = FirebaseStorage.getInstance().getReference();

        riversRef = mStorageRef.child(Name + "/inf.db");
        //riversRef1 = mStorageRef.child(Name + "/kig.db");
        riversRef2 = mStorageRef.child(Name + "/neuroosm.db");
        riversRef3 = mStorageRef.child(Name + "/training.db");
        riversRef4 = mStorageRef.child(Name + "/osm.db");
        R1 = (RadioButton) findViewById(R.id.r1);
        R2 = (RadioButton) findViewById(R.id.radioSwap2);
        R3 = (RadioButton) findViewById(R.id.radioSwap3);
        mButton = (Button) findViewById(R.id.NextBut);
        mButtonOut = (Button) findViewById(R.id.OutBut);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Action.equals("reg"))
                    if (R1.isChecked()) {
                        try {
                            //Toast.makeText(SucEnter.this, download().toString(), Toast.LENGTH_SHORT).show();
                            //download().toString();
                            download();
                            displayDatabaseInfo();
                        } catch (IOException e) {
                            Toast.makeText(SucEnter.this, "!", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                if (R2.isChecked()) {
                    Toast.makeText(SucEnter.this, "Успешной работы!", Toast.LENGTH_SHORT).show();
                    displayDatabaseInfo();
                }
                if (R3.isChecked()) {
                    //mExpimp.exportDB();
                    upload();
                    displayDatabaseInfo();
                }
            }
        });
        mButtonOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                Intent i = new Intent(SucEnter.this, LogInActivity.class);
                startActivity(i);
            }
        });
    }
    public void upload() {
        final Uri file = Uri.fromFile(new File(Environment.getDataDirectory(), "//data//" + "com.example.lukianmichael.WatchAndFit"
                + "//databases//" + "inf.db"));
        //final Uri file1 = Uri.fromFile(new File(Environment.getDataDirectory(), "//data//" + "com.example.lukianmichael.WatchAndFit"
          //      + "//databases//" + "kig.db"));
        final Uri file2 = Uri.fromFile(new File(Environment.getDataDirectory(), "//data//" + "com.example.lukianmichael.WatchAndFit"
                + "//databases//" + "neuroosm.db"));
        final Uri file3 = Uri.fromFile(new File(Environment.getDataDirectory(), "//data//" + "com.example.lukianmichael.WatchAndFit"
                + "//databases//" + "training.db"));
        final Uri file4 = Uri.fromFile(new File(Environment.getDataDirectory(), "//data//" + "com.example.lukianmichael.WatchAndFit"
                + "//databases//" + "osm.db"));
        Toast.makeText(SucEnter.this, "Идет загрузка", Toast.LENGTH_SHORT).show();
        riversRef.putFile(file)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        Toast.makeText(SucEnter.this, "Загрузка завершена успешно!", Toast.LENGTH_SHORT).show();
                        //Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Toast.makeText(SucEnter.this, "Ошибка!", Toast.LENGTH_SHORT).show();
                        // ...
                    }
                });

        riversRef2.putFile(file2)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        Toast.makeText(SucEnter.this, "Загрузка завершена успешно!", Toast.LENGTH_SHORT).show();
                        //Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Toast.makeText(SucEnter.this, "Ошибка!", Toast.LENGTH_SHORT).show();
                        // ...
                    }
                });
        riversRef3.putFile(file3)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        Toast.makeText(SucEnter.this, "Загрузка завершена успешно!", Toast.LENGTH_SHORT).show();
                        //Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Toast.makeText(SucEnter.this, "Ошибка!", Toast.LENGTH_SHORT).show();
                        // ...
                    }
                });
        riversRef4.putFile(file4)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        Toast.makeText(SucEnter.this, "Загрузка завершена успешно!", Toast.LENGTH_SHORT).show();
                        //Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Toast.makeText(SucEnter.this, "Ошибка!", Toast.LENGTH_SHORT).show();
                        // ...
                    }
                });
    }
    public void download() throws IOException {
        Toast.makeText(SucEnter.this, "Идет загрузка", Toast.LENGTH_SHORT).show();
        final File localFile = new File(Environment.getDataDirectory(), "//data//" + "com.example.lukianmichael.WatchAndFit"
                + "//databases//" + "inf.db");
        final File localFile2 = new File(Environment.getDataDirectory(), "//data//" + "com.example.lukianmichael.WatchAndFit"
                + "//databases//" + "neuroosm.db");
        final File localFile3 = new File(Environment.getDataDirectory(), "//data//" + "com.example.lukianmichael.WatchAndFit"
                + "//databases//" + "training.db");
        final File localFile4 = new File(Environment.getDataDirectory(), "//data//" + "com.example.lukianmichael.WatchAndFit"
                + "//databases//" + "osm.db");
        riversRef.getFile(localFile)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Toast.makeText(SucEnter.this, "Скачивание завершено!", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(SucEnter.this, exception.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        riversRef2.getFile(localFile2)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Toast.makeText(SucEnter.this, "Скачивание завершено!", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(SucEnter.this, exception.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        riversRef3.getFile(localFile3)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                        Toast.makeText(SucEnter.this, "Скачивание завершено!", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(SucEnter.this, exception.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        riversRef4.getFile(localFile4)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Toast.makeText(SucEnter.this, "Скачивание завершено!", Toast.LENGTH_SHORT).show();

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(SucEnter.this, exception.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void displayDatabaseInfo() {
        Cursor cursor = mDbHelper1.getReadableDatabase().query(TrainingContract.Information.TABLE_NAME1, new String[]{"_id", TrainingContract.Information.COLUMN_WEIGHT, TrainingContract.Information.COLUMN_HEIGH, TrainingContract.Information.COLUMN_AGE, TrainingContract.Information.COLUMN_GENDER}, null, null, null, null, "_id DESC");
        try {
            if (cursor.getCount() == 0) {
                Intent intent = new Intent(SucEnter.this, SettingActivity.class);
                startActivity(intent);
                finish();
            }
            else {
                Intent intent = new Intent(SucEnter.this, MainActivity.class);
                startActivity(intent);
                finish();

            }

        } finally {
            cursor.close();
        }
    }
}