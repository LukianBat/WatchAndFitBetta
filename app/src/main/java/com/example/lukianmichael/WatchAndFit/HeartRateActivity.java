package com.example.lukianmichael.WatchAndFit;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class HeartRateActivity extends AppCompatActivity implements SensorEventListener {
    String mPulse;
    Sensor mHeartRateSensor;
    SensorManager mSensorManager;
    TextView mTextViewPulse;
    TextView mTextViewPulseSit;
    TextView mTextViewPulseStand;
    String mPulseSit;
    String mPulseStand;
    int k=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heart_rate);
        mTextViewPulse = (TextView) findViewById(R.id.textViewPulse);
        mTextViewPulseSit = (TextView) findViewById(R.id.textViewPulseSit);
        mTextViewPulseStand = (TextView) findViewById(R.id.textViewPulseStand);

        mSensorManager = ((SensorManager)getSystemService(SENSOR_SERVICE));
        assert mSensorManager != null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            mHeartRateSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
        }
        else{
            Toast.makeText(HeartRateActivity.this,"Ваше устройство не поддерживает данную фнукцию!", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        //Register the listener
        if (mSensorManager != null){
            mSensorManager.registerListener(this, mHeartRateSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Unregister the listener
        if (mSensorManager!=null)
            mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        //Update your data.
        if (event.sensor.getType() == Sensor.TYPE_HEART_RATE) {
            int heartrate = (int) event.values[0];
            mPulse = String.valueOf(heartrate);
            mTextViewPulse.setText(mPulse);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
    public void onClickAdd(View view) {
        if (k==0){
            mTextViewPulseSit.setText("Пульс сидя:"+mPulse);
            mPulseSit=mPulse;
        }else {
            mTextViewPulseStand.setText("Пульс стоя:"+mPulse);
            mPulseStand=mPulse;
            Intent i = new Intent(HeartRateActivity.this, AddActivity.class);
            i.putExtra("sit", mPulseSit);
            i.putExtra("stand", mPulseStand);
            startActivity(i);
        }
        k++;
    }
}
