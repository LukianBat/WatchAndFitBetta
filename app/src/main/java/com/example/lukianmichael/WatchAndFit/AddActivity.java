package com.example.lukianmichael.WatchAndFit;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.lukianmichael.WatchAndFit.data.NeuroOsmDbHelper;
import com.example.lukianmichael.WatchAndFit.data.OsmDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.Calendar;

import static java.lang.Math.abs;
public class AddActivity extends AppCompatActivity {
    boolean indexTime = false;
    private NeuroOsmDbHelper mDbHelper1;
    private EditText mPsit;
    private EditText mPstand;
    double a=0;
    float Result;
    private ImageView mImageView;
    private TextView mResult, mZone;
    private OsmDbHelper mDbHelper;
    TextView mTextView;
    TextView mTextViewtitle;
    int Psit, Pstand, Zone;
    String date;
    String Name;
    private double W;
    private ScrollView mScrollView;
    private TextView mPulseArduino;
    private ImageButton mButton2;
    private ImageButton mButton1;
    StorageReference riversRef2;
    StorageReference riversRef4;
    private StorageReference mStorageRef;
    FirebaseUser user;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    final String[] mChooseVar = { "Хорошее", "Нормальное", "Плохое", "Очень плохое"};
    /*
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == share) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_TEXT, sendData());
            i.putExtra(Intent.EXTRA_SUBJECT, "Отправка результатов");
            i = Intent.createChooser(i, "Отправить с помощью");
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/
    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        mButton1 = (ImageButton) findViewById(R.id.buttonPulse);
        mButton2 = (ImageButton) findViewById(R.id.button12);
        mPulseArduino = (TextView) findViewById(R.id.textViewPulse);
        mScrollView = (ScrollView) findViewById(R.id.ScrollView);
        mDbHelper1 = new NeuroOsmDbHelper(this);
        mScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        mPulseArduino = (TextView) findViewById(R.id.textViewPulse);
        mPsit = (EditText) findViewById(R.id.editText);
        mPstand = (EditText) findViewById(R.id.editText2);
        mResult = (TextView) findViewById(R.id.textView6);
        mZone = (TextView) findViewById(R.id.textView9);
        final ImageButton mButton = (ImageButton) findViewById(R.id.button);
        mTextView = (TextView) findViewById(R.id.textView24);
        mTextViewtitle = (TextView) findViewById(R.id.textView25);
        mImageView = (ImageView) findViewById(R.id.imageView);
        mDbHelper = new OsmDbHelper(this);
        final Intent intent = getIntent();
        String Sit = intent.getStringExtra("sit");
        String Stand = intent.getStringExtra("stand");
        Log.i("log", "Значения пульса="+Sit+"  "+Stand);
        if (Sit!=null){
            mPsit.setText(Sit);
        }
        if (Stand!=null){
            mPstand.setText(Stand);
        }
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Name = user.getEmail();
                } else {
                    Toast.makeText(AddActivity.this, "Нет соединения с интернетом, или вы не авторизированы!", Toast.LENGTH_SHORT).show();
                }
            }
        };

        Calendar c;
        c = Calendar.getInstance();
        if ((c.get(c.MONTH)+1)>=10){
            date = String.valueOf(c.get(c.DAY_OF_MONTH)) + ":" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
        }else {
            if ((c.get(c.MONTH) + 1) < 10) {
                date = String.valueOf(c.get(c.DAY_OF_MONTH)) + ":0" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
            }
            if (c.get(c.DAY_OF_MONTH) < 10) {
                date = "0" + String.valueOf(c.get(c.DAY_OF_MONTH)) + ":" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
            }
            if ((c.get(c.MONTH) + 1) < 10) {
                date = String.valueOf(c.get(c.DAY_OF_MONTH)) + ":0" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
            }
            if ((c.get(c.DAY_OF_MONTH) < 10) && ((c.get(c.MONTH) + 1) < 10)) {
                date = "0" + String.valueOf(c.get(c.DAY_OF_MONTH)) + ":0" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
            }
        }
        mPstand.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mButton.setClickable(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mPsit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mButton.setClickable(true);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(AddActivity.this);
                builder.setTitle("Состояние организма по вашей оценке")
                        .setCancelable(true)
                        .setItems(mChooseVar,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int item) {
                                        switch (item){
                                            case 0:
                                                a=7.5;
                                                Log.i("log", "W-begin"+String.valueOf(W));
                                                NeuroPoint();
                                                dialog.cancel();
                                                finish();
                                                Intent intent = new Intent(AddActivity.this, MainActivity.class);
                                                startActivity(intent);
                                                break;
                                            case 1:
                                                Log.i("log", "W-begin"+String.valueOf(W));                                                a=5.5;
                                                NeuroPoint();
                                                dialog.cancel();
                                                finish();
                                                Intent intent1 = new Intent(AddActivity.this, MainActivity.class);
                                                startActivity(intent1);
                                                break;
                                            case 2:
                                                Log.i("log", "W-begin"+String.valueOf(W));                                                a=3.25;
                                                NeuroPoint();
                                                dialog.cancel();
                                                finish();
                                                Intent intent2 = new Intent(AddActivity.this, MainActivity.class);
                                                startActivity(intent2);
                                                break;
                                            case 3:
                                                Log.i("log", "W-begin"+String.valueOf(W));                                                a=2;
                                                NeuroPoint();
                                                dialog.cancel();
                                                finish();
                                                Intent intent3 = new Intent(AddActivity.this, MainActivity.class);
                                                startActivity(intent3);
                                                break;
                                        }
                                        mStorageRef = FirebaseStorage.getInstance().getReference();
                                        riversRef2 = mStorageRef.child(Name + "/neuroosm.db");
                                        riversRef4 = mStorageRef.child(Name + "/osm.db");
                                        upload();
                                        finish();
                                    }
                                });

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });
        mButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddActivity.this, HeartRateActivity.class);
                startActivity(i);
           }
        });
        mButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (mPsit.getText().toString().equals("") || mPstand.getText().toString().equals("")) {
                    mResult.setText("Введите значения !");
                    mZone.setText("");
                } else {
                    mZone.setVisibility(View.VISIBLE);
                    mPulseArduino.setVisibility(View.GONE);
                    mImageView.setVisibility(View.VISIBLE);
                    Psit = Integer.parseInt(mPsit.getText().toString());
                    Pstand = Integer.parseInt(mPstand.getText().toString());
                    displayDatabaseW();
                    Result = (float) (W*((14.5 - 0.5 * (Psit - 40) / 3.5 - (Pstand - Psit) / 2.27 * 0.5)));
                    mResult.setText("Балл:  " + String.format("%.2f", Result));
                    Toast.makeText(getApplicationContext(), "Данные сохранены", Toast.LENGTH_SHORT).show();
                    if (Result > 7.5) {
                        Zone = 1;
                        mImageView.setImageResource(R.drawable.one_green);
                        mZone.setText("зона");
                        mTextViewtitle.setText(R.string.zone_1t);
                        mTextView.setText(R.string.zone_1);
                    }
                    if (Result <= 7.5 && Result > 4.5) {
                        Zone = 2;
                        mImageView.setImageResource(R.drawable.two_blue);
                        mZone.setText("зона");
                        mTextViewtitle.setText(R.string.zone_2t);
                        mTextView.setText(R.string.zone_2);


                    }
                    if (Result <= 4.5 && Result >= 2) {
                        Zone = 3;
                        mImageView.setImageResource(R.drawable.three_yellow);
                        mZone.setText("зона");
                        mTextViewtitle.setText(R.string.zone_3t);
                        mTextView.setText(R.string.zone_3);

                    }
                    if (Result < 2) {
                        Zone = 4;
                        mImageView.setImageResource(R.drawable.four_red);
                        mZone.setText("зона");
                        mTextViewtitle.setText(R.string.zone_4t);
                        mTextView.setText(R.string.zone_4);

                    }
                    insertOsm();
                    mButton.setClickable(false);
                    mButton2.setVisibility(View.VISIBLE);
                    mScrollView.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            return false;
                        }
                    });
                    mScrollView.scrollTo(mTextView.getScrollX(), mTextView.getScrollY() + 10000);
                }
            }
        });
    }


    private String sendData() {
        String sendNote = "Запись ОСМ " + String.valueOf(date) + ";\n" + " ЧСС сидя: " + String.valueOf(Psit) + ";\n" + " ЧСС стоя: " + String.valueOf(Pstand) + ";\n" + " Балл: " + String.format("%.2f", Result) + ";\n" + " Зона: " + String.valueOf(Zone);
        return sendNote;
    }

    private void insertOsm() {
        SQLiteDatabase db = this.mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TrainingContract.OSM.COLUMN_PSIT, Integer.valueOf(Psit));
        values.put(TrainingContract.OSM.COLUMN_PSTAND, Integer.valueOf(Pstand));
        values.put(TrainingContract.OSM.COLUMN_POINT, Float.valueOf(Result));
        values.put(TrainingContract.OSM.COLUMN_ZONE, Integer.valueOf(Zone));
        values.put(TrainingContract.OSM.COLUMN_DATE, String.valueOf(date));
        long newRowId = db.insert(TrainingContract.OSM.TABLE_NAME2, null, values);
    }


    public void NeuroPoint(){
        if (a<Result) {

            double out = abs(a/Result);
            Log.i("log", "Out="+String.valueOf(out)+" ");
            if (a!=7.5) {
                if (0 <= out && out < 0.7) {
                    W = W - 0.06;
                }
                if (0.7 <= out && out < 0.8) {
                    W = W - 0.03;

                }
                if (0.8 <= out && out < 0.9) {
                    W = W - 0.02;


                }
                if (0.9 <= out && out <= 1) {
                    W = W - 0.01;
                }
            }
        }
        if (a>Result && a!=2){
            double out = abs(Result/a);
            Log.i("log", "Out="+String.valueOf(out)+" ");
            if (0 <= out && out < 0.7) {
                W = W + 0.06;
            }
            if (0.7 <= out && out < 0.8) {
                W = W + 0.03;
            }
            if (0.8 <= out && out < 0.9) {
                W = W + 0.02;
            }
            if (0.9 <= out && out <= 1) {
                W = W + 0.01;
            }
        }
        insertW();
    }
    public void displayDatabaseW() {
        Cursor cursor = mDbHelper1.getReadableDatabase().query(TrainingContract.NeuroOSM.TABLE_NAME4, new String[]{"_id", TrainingContract.NeuroOSM.COLUMN_W}, null, null, null, null, "_id ASC");
        try {
            if (cursor.getCount() == 0) {
                W=1;
            }else {
                int idColumnIndex = cursor.getColumnIndex("_id");
                int wColumnIndex = cursor.getColumnIndex(TrainingContract.NeuroOSM.COLUMN_W);
                while (cursor.moveToNext()) {
                    int currentID = cursor.getInt(idColumnIndex);
                    Float currentW = cursor.getFloat(wColumnIndex);
                    W=currentW;
                    Log.i("log", "Data W-end="+String.valueOf(W)+" ");
                }
            }
        } finally {
            cursor.close();
        }
    }
    private void insertW() {
        SQLiteDatabase db = this.mDbHelper1.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TrainingContract.NeuroOSM.COLUMN_W, W);
        long newRowId = db.insert(TrainingContract.NeuroOSM.TABLE_NAME4, null, values);
    }
    public void upload() {

        final Uri file2 = Uri.fromFile(new File(Environment.getDataDirectory(), "//data//" + "com.example.lukianmichael.WatchAndFit"
                + "//databases//" + "neuroosm.db"));
        final Uri file4 = Uri.fromFile(new File(Environment.getDataDirectory(), "//data//" + "com.example.lukianmichael.WatchAndFit"
                + "//databases//" + "osm.db"));
        Toast.makeText(AddActivity.this, "Идет загрузка в облако", Toast.LENGTH_SHORT).show();

        riversRef2.putFile(file2)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        Toast.makeText(AddActivity.this, "Загрузка завершена успешно!", Toast.LENGTH_SHORT).show();
                        //Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Toast.makeText(AddActivity.this, "Ошибка!", Toast.LENGTH_SHORT).show();
                        // ...
                    }
                });
        riversRef4.putFile(file4)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        Toast.makeText(AddActivity.this, "Загрузка завершена успешно!", Toast.LENGTH_SHORT).show();
                        //Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Toast.makeText(AddActivity.this, "Ошибка!", Toast.LENGTH_SHORT).show();
                        // ...
                    }
                });
    }

}