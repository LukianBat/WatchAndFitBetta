package com.example.lukianmichael.WatchAndFit;

/**
 * Created by ViP on 28.06.2017.
 */

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class RVAdapterTraining extends RecyclerView.Adapter<RVAdapterTraining.PersonViewHolder> {

    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView kalCount;
        TextView Time;
        TextView srPulse;
        TextView mdate;
        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv2);

            kalCount = (TextView)itemView.findViewById(R.id.kalCount);
            Time = (TextView)itemView.findViewById(R.id.Time);
            srPulse = (TextView)itemView.findViewById(R.id.Pulse);
            mdate= (TextView) itemView.findViewById(R.id.textViewDate1);
        }
    }

    List<TrainingData> TrainingDatas;
    RVAdapterTraining(List<TrainingData> TrainingDatas){
        this.TrainingDatas = TrainingDatas;
    }
    @Override
    public int getItemCount() {
        return TrainingDatas.size();
    }
    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_training, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }
    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.kalCount.setText("Количество потраченной энергии: "+String.format("%.2f",(TrainingDatas.get(i).Energy/1000.0))+" Ккал");
        personViewHolder.Time.setText("Время измерений: "+String.format("%.2f",((TrainingDatas.get(i).Time) / 60.0))+" мин.");
        personViewHolder.srPulse.setText("Средний пульс за тренировку: "+ TrainingDatas.get(i).srPulse+" уд. в мин.");
        personViewHolder.mdate.setText("Дата тренировки: "+ TrainingDatas.get(i).date);
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}