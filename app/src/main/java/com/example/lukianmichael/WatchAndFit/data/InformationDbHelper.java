package com.example.lukianmichael.WatchAndFit.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class InformationDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "inf.db";
    private static final int DATABASE_VERSION = 1;
    public static final String LOG_TAG = InformationDbHelper.class.getSimpleName();

    public InformationDbHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS inf (_id INTEGER PRIMARY KEY AUTOINCREMENT, weight INTEGER NOT NULL, heigh INTEGER NOT NULL, age INTEGER NOT NULL, gender INTEGER NOT NULL );");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("SQLite", "Обновляемся с версии " + oldVersion + " на версию " + newVersion);

        // Удаляем старую таблицу и создаём новую
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME);
        // Создаём новую таблицу
        onCreate(db);
    }

}
