package com.example.lukianmichael.WatchAndFit;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lukianmichael.WatchAndFit.data.KigDbHelper;
import com.example.lukianmichael.WatchAndFit.data.NeuroKigDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import static java.lang.Math.abs;

public class KardioIntervalActivity extends Fragment {
    /////////
    String c;
    //////////
    TextView txtArduino;
    private static final int REQUEST_ENABLE_BT = 1;
    final int RECIEVE_MESSAGE = 1;
    private BluetoothSocket btSocket = null;
    private BluetoothAdapter bluetooth;
    private BroadcastReceiver discoverDevicesReceiver;
    private BroadcastReceiver discoveryFinishedReceiver;
    private ProgressDialog progressDialog;
    private static String address="";
    private String name="";
    public Handler h;
    private ConnectedThread mConnectedThread;
    private Timer mTimer;
    private MyTimerTask mMyTimerTask;
    ResultsKIG frag1;
    double a;
    FragmentTransaction fTrans;
    double MasBeep1[] = new double[10000];
    double MasBeep[] = new double[10000];
    double MasBeepCurrent[] = new double [10000];
    double MasBeepIndex[]  = new double [10000];
    int counter=0;
    int counter1=0;
    double Mo;
    float AMo;
    double DX;
    float In;
    private double W=1;
    ///////
    boolean timeIndex=false;
    private KigDbHelper mDbHelper;
    double srPulse=0;
    int PULSE;
    double BEEP;
    String date;
    long Time = 0;
    TextView mTextView;
    private Chronometer mChronometer;
    private Button mButton;
    int x=0;
    int y=0;
    int x0=0;
    int y0=0;
    GraphView graph;
    private NeuroKigDbHelper mDbHelper1;
    @RequiresApi(api = Build.VERSION_CODES.N)
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_kardio_interval, container, false);
        graph = (GraphView) rootView.findViewById(R.id.graph1);
        txtArduino = (TextView) rootView.findViewById(R.id.textView281);
        mDbHelper = new KigDbHelper(getActivity());
        mDbHelper1 = new NeuroKigDbHelper(getActivity());
        /////////////////// НЕ ЗАБУДЬ ПОМЕНЯТЬ ЗНАЧЕНИЕ SETTEXT НА НУЛЕВОЕ

        ////////////;
        mTextView = (TextView)rootView.findViewById(R.id.textView201);
        ///////////////////////////////////

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            BluetoothManager mbluetoothManager = (BluetoothManager) getActivity().getSystemService(Context.BLUETOOTH_SERVICE); // на версиях начиная JELLY_BEAN_MR2 д
            bluetooth = mbluetoothManager.getAdapter();
        } else {
            bluetooth = BluetoothAdapter.getDefaultAdapter();
        }
        String status = null;
        if(bluetooth.isEnabled()){
            String mydeviceaddress= bluetooth.getAddress();
            String mydevicename= bluetooth.getName();
            status="Ваши данные:"+mydevicename+" : "+ mydeviceaddress;
            Toast.makeText(getContext(), status, Toast.LENGTH_SHORT).show();
            discoverDevices();
        }
        else
        {Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(0,0)
        });
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(1024);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(10);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        graph.getGridLabelRenderer().setVerticalLabelsVisible(false);
        // enable scaling and scrolling
        graph.getViewport().setScalable(true);
        graph.getViewport().setScalableY(false);
        graph.addSeries(series);
        mChronometer = (Chronometer) rootView.findViewById(R.id.chronometer11);
        mButton = (Button)rootView.findViewById(R.id.button21);
        boolean SetttingTime=false;
        frag1 = new ResultsKIG();
        mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long elapsedMillis = SystemClock.elapsedRealtime()
                        - mChronometer.getBase();
                if (elapsedMillis>=3000){
                    timeIndex = true;
                }
                    if (elapsedMillis<3000) {
                        mTextView.setText("Идёт настройка приёма данных!");
                        srPulse=0;
                    }
                    if(elapsedMillis>=3000) {
                        Time = ((elapsedMillis-3000) / 1000);
                        mTextView.setText("Время: " + Time + "сек Пульс: " + PULSE);
                        srPulse = srPulse + PULSE;
                    }
                    if (Time>=60){
                        chronometer.stop();
                        mTimer.cancel();
                        mMyTimerTask.cancel();
                        try {
                            mConnectedThread.interrupt();
                            btSocket.close();
                            bluetooth.cancelDiscovery();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            getActivity().unregisterReceiver(discoverDevicesReceiver);
                            getActivity().unregisterReceiver(discoveryFinishedReceiver);
                        }catch (IllegalArgumentException e){

                        }
                            discoverDevicesReceiver.clearAbortBroadcast();
                        discoveryFinishedReceiver.clearAbortBroadcast();
                        insertKIG();
                        final String[] mChooseVar = { "Хорошее", "Нормальное", "Плохое"};
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("Состояние организма по вашей оценке")
                                    .setCancelable(false)
                                    .setItems(mChooseVar,
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog,int item) {
                                                    switch (item){
                                                        case 0:
                                                            a=80;
                                                            NeuroPoint();
                                                            dialog.cancel();
                                                            break;
                                                        case 1:
                                                            a=65;
                                                            NeuroPoint();
                                                            dialog.cancel();
                                                            break;
                                                        case 2:
                                                            a=50;
                                                            NeuroPoint();
                                                            dialog.cancel();
                                                            break;
                                                    }
                                                    insertValueKig();
                                                    /*
                                                    fTrans = getFragmentManager().beginTransaction();
                                                    fTrans.replace(R.id.container, frag1);
                                                    fTrans.commit();
                                                    */
                                                    Toast.makeText(getActivity(),"Спасибо", Toast.LENGTH_SHORT).show();
                                                }
                                            });

                            AlertDialog dialog = builder.create();
                            dialog.show();
                    }
            }
        });
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bluetooth.isEnabled()) {
                    if (address!="") {
                        mButton.setClickable(false);
                        if (mTimer != null) {
                            mTimer.cancel();
                        }
                        mTimer = new Timer();
                        mMyTimerTask = new MyTimerTask();
                        mTimer.schedule(mMyTimerTask, 0, 35);
                        mChronometer.setBase(SystemClock.elapsedRealtime());
                        mChronometer.start();


                    }
                    else{
                        Toast.makeText(getContext(),"Устройство не подключено, попробуйте заново подключиться!",Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        h = new Handler() {
            public void handleMessage(android.os.Message msg) {
                String pulse = "";
                String anal = "";
                String ki = "";
                int buf=0;
                int countbuf=0;
                switch (msg.what) {
                    case RECIEVE_MESSAGE:
                        int error = 0;
                        if(timeIndex==true) {
                            if (c.length()==14) {
                                for (int i = 0; i < c.length(); i++) {
                                    if (c.charAt(i) == ' ' && buf == 0) {
                                        anal = c.substring(countbuf, i);
                                        countbuf = i + 1;
                                        buf++;
                                        continue;
                                    }
                                    if (c.charAt(i) == ' ' && buf == 1) {
                                        pulse = c.substring(countbuf, i);
                                        countbuf = i + 1;
                                        buf++;
                                        continue;
                                    }
                                    if (c.charAt(i) == ' ' && buf == 2) {
                                        ki = c.substring(countbuf, i);
                                        countbuf = i + 1;
                                        buf++;
                                    }
                                }
                                try {
                                    y = Integer.valueOf(anal);
                                    PULSE = Integer.valueOf(pulse);
                                    BEEP = Double.valueOf(ki);
                                    if (counter1>0) {
                                        counter1++;
                                        MasBeep1[counter1] = BEEP;
                                        Log.i("log", "beep=" + BEEP / 10.0);
                                    }else{
                                        counter1++;
                                        MasBeep1[0]=7;
                                    }
                                }catch (NumberFormatException e){

                                }
                            }
                            else{
                                y=y0;
                                PULSE=80;
                            }
                        }
                        break;
                }
            };
        };
        Calendar c= Calendar.getInstance();
        if ((c.get(c.MONTH)+1)<10) {
            date = String.valueOf(c.get(c.DAY_OF_MONTH)) + ":0" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
        }
        if (c.get(c.DAY_OF_MONTH)<10) {
            date = "0"+String.valueOf(c.get(c.DAY_OF_MONTH)) + ":" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
        }
        if ((c.get(c.MONTH)+1)<10) {
            date = String.valueOf(c.get(c.DAY_OF_MONTH)) + ":0" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
        }
        if ((c.get(c.DAY_OF_MONTH)<10) && ((c.get(c.MONTH)+1)<10)) {
            date = "0"+String.valueOf(c.get(c.DAY_OF_MONTH)) + ":0" + String.valueOf(c.get(c.MONTH) + 1) + ":" + String.valueOf(c.get(c.YEAR));
        }
        return rootView;
    }
    public void discoverDevices() {
        if (discoverDevicesReceiver == null) {
            if (discoverDevicesReceiver == null) {
                discoverDevicesReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String action = intent.getAction();
                        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                            if (String.valueOf(device.getName()).equals("BT_test")){
                                name = String.valueOf(device.getName());
                                address=String.valueOf(device.getAddress());
                                progressDialog.dismiss();
                                bluetooth.cancelDiscovery();

                                Connection();

                            }
                        }
                    }


                };
            }
            if (discoveryFinishedReceiver == null) {
                discoveryFinishedReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if (progressDialog != null)
                            progressDialog.dismiss();
                        if (!name.equals("BT_test")) {
                            Toast.makeText(getContext(), "Устройства не найдено", Toast.LENGTH_LONG).show();
                        }
                        getActivity().unregisterReceiver(discoveryFinishedReceiver);

                    }
                };
            }

            getActivity().registerReceiver(discoverDevicesReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
            getActivity().registerReceiver(discoveryFinishedReceiver, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));
            progressDialog = ProgressDialog.show(getActivity(), "Поиск устройств", "Подождите...");
            bluetooth.startDiscovery();

        }
    }
    private void Connection() {
        //Пытаемся проделать эти действия
        try {
            //Устройство с данным адресом - наш Bluetooth Bee
            //Адрес опредеяется следующим образом: установите соединение
            //между ПК и модулем (пин: 1234), а затем посмотрите в настройках
            //соединения адрес модуля. Скорее всего он будет аналогичным.
            BluetoothDevice device = bluetooth.getRemoteDevice(address);

            //Инициируем соединение с устройством
            Method m = device.getClass().getMethod(
                    "createRfcommSocket", new Class[]{int.class});
            btSocket = (BluetoothSocket) m.invoke(device, 1);
            btSocket.connect();

            //В случае появления любых ошибок, выводим в лог сообщение
        } catch (IOException e) {
            Log.d("BLUETOOTH", e.getMessage());
        } catch (SecurityException e) {
            Log.d("BLUETOOTH", e.getMessage());
        } catch (NoSuchMethodException e) {
            Log.d("BLUETOOTH", e.getMessage());
        } catch (IllegalArgumentException e) {
            Log.d("BLUETOOTH", e.getMessage());
        } catch (IllegalAccessException e) {
            Log.d("BLUETOOTH", e.getMessage());
        } catch (InvocationTargetException e) {
            Log.d("BLUETOOTH", e.getMessage());
        }
        //Выводим сообщение об успешном подключении
//        Toast.makeText(getContext(), "CONNECTED", Toast.LENGTH_LONG).show();
        try {
            //Получаем выходной поток для передачи данных
            OutputStream outStream = btSocket.getOutputStream();
            byte value = 1;
            //В зависимости от того, какая кнопка была нажата,
            //изменяем данные для посылки
            //Пишем данные в выходной поток
            outStream.write(value);
            Log.i("log", "output");
        } catch (IOException e) {
            //Если есть ошибки, выводим их в лог
            Log.d("Не работает", e.getMessage());
        }
        try {
        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();
        }catch (NullPointerException e)
        {
            try {

                BluetoothDevice device = bluetooth.getRemoteDevice(address);
                //Инициируем соединение с устройством
                Method m = device.getClass().getMethod(
                        "createRfcommSocket", new Class[]{int.class});
                btSocket = (BluetoothSocket) m.invoke(device, 1);
                btSocket.connect();

                //В случае появления любых ошибок, выводим в лог сообщение
            } catch (IOException e2) {
                Log.d("BLUETOOTH", e.getMessage());
            } catch (SecurityException e2) {
                Log.d("BLUETOOTH", e.getMessage());
            } catch (NoSuchMethodException e2) {
                Log.d("BLUETOOTH", e.getMessage());
            } catch (IllegalArgumentException e2) {
                Log.d("BLUETOOTH", e.getMessage());
            } catch (IllegalAccessException e2) {
                Log.d("BLUETOOTH", e.getMessage());
            } catch (InvocationTargetException e2) {
                Log.d("BLUETOOTH", e.getMessage());
            }
            //Выводим сообщение об успешном подключении
//        Toast.makeText(getContext(), "CONNECTED", Toast.LENGTH_LONG).show();
            try {
                //Получаем выходной поток для передачи данных
                OutputStream outStream = btSocket.getOutputStream();
                byte value = 1;
                //В зависимости от того, какая кнопка была нажата,
                //изменяем данные для посылки
                //Пишем данные в выходной поток
                outStream.write(value);
                Log.i("log", "output");
            } catch (IOException e1) {
                //Если есть ошибки, выводим их в лог
                Log.d("Не работает", e.getMessage());
            }
        }
    }

    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {

            }

            mmInStream = tmpIn;
        }

        public void run() {
            byte[] buffer = new byte[256];  // buffer store for the stream
                try {
                    mmInStream.read(buffer);
                    c = new String(buffer).substring(0, 14);
                   // Log.i("log", c);
                    if (!c.contains("\r") && !c.contains("\n")) {
                        h.obtainMessage(RECIEVE_MESSAGE).sendToTarget();
                    }

                } catch (IOException e) {
                    Log.i("log",e.toString());
                }

        }

    }
    class MyTimerTask extends TimerTask {

        @Override
        public void run() {

            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    try {
                        mConnectedThread = new ConnectedThread(btSocket);
                        mConnectedThread.start();
                    }catch (NullPointerException e){
                        try {

                            BluetoothDevice device = bluetooth.getRemoteDevice(address);
                            //Инициируем соединение с устройством
                            Method m = device.getClass().getMethod(
                                    "createRfcommSocket", new Class[]{int.class});
                            btSocket = (BluetoothSocket) m.invoke(device, 1);
                            btSocket.connect();

                            //В случае появления любых ошибок, выводим в лог сообщение
                        } catch (IOException e2) {
                            Log.d("BLUETOOTH", e.getMessage());
                        } catch (SecurityException e2) {
                            Log.d("BLUETOOTH", e.getMessage());
                        } catch (NoSuchMethodException e2) {
                            Log.d("BLUETOOTH", e.getMessage());
                        } catch (IllegalArgumentException e2) {
                            Log.d("BLUETOOTH", e.getMessage());
                        } catch (IllegalAccessException e2) {
                            Log.d("BLUETOOTH", e.getMessage());
                        } catch (InvocationTargetException e2) {
                            Log.d("BLUETOOTH", e.getMessage());
                        }
                        //Выводим сообщение об успешном подключении
//        Toast.makeText(getContext(), "CONNECTED", Toast.LENGTH_LONG).show();
                        try {
                            //Получаем выходной поток для передачи данных
                            OutputStream outStream = btSocket.getOutputStream();
                            byte value = 1;
                            //В зависимости от того, какая кнопка была нажата,
                            //изменяем данные для посылки
                            //Пишем данные в выходной поток
                            outStream.write(value);
                            Log.i("log", "output");
                        } catch (IOException e1) {
                            //Если есть ошибки, выводим их в лог
                            Log.d("Не работает", e.getMessage());
                        }
                    }

                    long elapsedMillis = SystemClock.elapsedRealtime()
                            - mChronometer.getBase();
                    LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[]{
                            new DataPoint(x0,y0),
                            new DataPoint(x,y)
                    });
                    graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
                    graph.getGridLabelRenderer().setVerticalLabelsVisible(false);
                    graph.getViewport().setYAxisBoundsManual(true);
                    graph.getViewport().setMinY(0);
                    graph.getViewport().setMaxY(1024);

                    graph.getViewport().setXAxisBoundsManual(true);
                    graph.getViewport().setMinX(0);
                    graph.getViewport().setMaxX(10);

                    // enable scaling and scrolling
                    graph.getViewport().setScalable(true);
                    graph.getViewport().setScalableY(false);
                    graph.addSeries(series);
                    graph.getViewport().setXAxisBoundsManual(true);
                    if (x<5){
                        graph.getViewport().setMinX(0);
                        graph.getViewport().setMaxX(x+5);
                    }
                    else {
                        graph.getViewport().setMinX(x - 5);
                        graph.getViewport().setMaxX(x + 5);
                    }
                    x0=x;
                    y0=y;
                    x++;
                }
            });
        }
    }
    private void insertKIG() {
        displayDatabaseW();
        for (int i=1; i<counter1;i++){
            if (MasBeep1[i]<MasBeep1[i-1]){
                MasBeep[counter]=MasBeep1[i-1];
                counter++;
            }
        }
        for (int i=0; i<counter; i++){
            Log.i("log", MasBeep[i]+" ");
        }
        double max=0;
        for (int i=0; i<counter; i++)
        {
            if (MasBeep[i]>=max){
                max=MasBeep[i];
            }
        }
        double min=30;
        for (int i=0; i<counter; i++)
        {
            if (MasBeep[i]<min && MasBeep[i]>0){
                min=MasBeep[i];
            }
        }
        DX=(max-min)/10.0;
        for (int k=0; k<counter; k++)
        {
            MasBeepCurrent[k]=MasBeep[k];
            MasBeepIndex[k]++;
            for (int j= 0; j<k; j++){
                if (MasBeep[k]==MasBeepCurrent[j]){
                    MasBeepIndex[j]++;
                    MasBeep[k]=-1;
                }
            }
        }

        double maxindex = 0;
        double maxcurrent = 0;
        for (int i=0; i<counter; i++){
            if (MasBeepIndex[i]>maxindex){
                maxindex=MasBeepIndex[i];
                maxcurrent=MasBeepCurrent[i];
            }
        }
        Mo=maxcurrent/10.0;
        AMo= (float) (((maxindex)/counter*1.0)*100);
        In= ((float) ((AMo/(2*Mo*DX))));
        ////
        Log.i("log", "----------------------------------");
        Log.i("log", "max="+max);
        Log.i("log", "min="+min/10.0);
        Log.i("log", "dx="+DX);
        Log.i("log", "mo="+Mo);
        Log.i("log", "maxindex="+maxindex);
        Log.i("log", "amo="+AMo);
        Log.i("log", "counter="+counter);
        Log.i("log", "In="+In);
        Log.i("log", "----------------------------------");
        //////
    }
    public void insertValueKig(){
        SQLiteDatabase db = this.mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TrainingContract.KIGNotes.COLUMN_AMO, Float.valueOf(AMo));
        values.put(TrainingContract.KIGNotes.COLUMN_DATE, String.valueOf(date));
        values.put(TrainingContract.KIGNotes.COLUMN_DX, Double.valueOf(DX*10));
        values.put(TrainingContract.KIGNotes.COLUMN_MO,Double.valueOf(Mo*10));
        values.put(TrainingContract.KIGNotes.COLUMN_INDEXPOWER, Float.valueOf(In));
        long newRowId = db.insert(TrainingContract.KIGNotes.TABLE_NAME3, null, values);
    }
    public void displayDatabaseW() {
        Cursor cursor = mDbHelper1.getReadableDatabase().query(TrainingContract.NeuroKIG.TABLE_NAME5, new String[]{"_id", TrainingContract.NeuroOSM.COLUMN_W}, null, null, null, null, "_id DESC");
        try {
            if (cursor.getCount() == 0) {
                W=1;
            }else {
                int idColumnIndex = cursor.getColumnIndex("_id");
                int wColumnIndex = cursor.getColumnIndex(TrainingContract.NeuroOSM.COLUMN_W);
                while (cursor.moveToNext()) {
                    int currentID = cursor.getInt(idColumnIndex);
                    Float currentW = cursor.getFloat(wColumnIndex);
                    W=currentW;
                    Log.i("log", "сохраненное W="+W);
                }
            }
        } finally {
            cursor.close();
        }
    }

    public void NeuroPoint(){
        if (a<In) {

            double out = abs(a/In);
            if (a!=80) {
                if (0 <= out && out < 0.7) {
                    W = W - 0.06;
                }
                if (0.7 <= out && out < 0.8) {
                    W = W - 0.03;

                }
                if (0.8 <= out && out < 0.9) {
                    W = W - 0.02;
                }
                if (0.9 <= out && out <= 1) {
                    W = W - 0.01;
                }
            }
        }
        if (a>In && a!=50){
            double out = abs(In/a);
            if (0 <= out && out < 0.7) {
                W = W + 0.06;
            }
            if (0.7 <= out && out < 0.8) {
                W = W + 0.03;
            }
            if (0.8 <= out && out < 0.9) {
                W = W + 0.02;
            }
            if (0.9 <= out && out <= 1) {
                W = W + 0.01;
            }
        }
        SQLiteDatabase db = this.mDbHelper1.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TrainingContract.NeuroKIG.COLUMN_W, W);
        Log.i("log", "текущее W="+W);
        long newRowId = db.insert(TrainingContract.NeuroKIG.TABLE_NAME5, null, values);
    }
}