package com.example.lukianmichael.WatchAndFit;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.lukianmichael.WatchAndFit.data.InformationDbHelper;
import com.example.lukianmichael.WatchAndFit.data.OsmDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;
import com.example.lukianmichael.WatchAndFit.data.TrainingDbHelper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;

public class SettingActivity extends AppCompatActivity {
    private EditText EditTextAge;
    private EditText EditTextHeigh;
    private EditText EditTextWeight;
    int Gender;
    String Name;
    // private TextView display;
    //private TextView display1;
    private ImageButton mButton;
    public TrainingDbHelper mDbHelper;
    private InformationDbHelper mDbHelper1;
    private OsmDbHelper mDbHelper2;
    ImageButton mradioButtonM;
    ImageButton mradioButtonW;
    StorageReference riversRef;
    private StorageReference mStorageRef;
    FirebaseUser user;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.RadioGroup);
        mradioButtonM = (ImageButton) findViewById(R.id.radioButtonM);
        mradioButtonW = (ImageButton) findViewById(R.id.radioButtonW);
        //display = (TextView) rootView.findViewById(R.id.textView25);
        //display1 = (TextView) rootView.findViewById(R.id.textView26);
        mButton = (ImageButton) findViewById(R.id.button7);
        EditTextWeight = (EditText) findViewById(R.id.editTextWeight);
        EditTextHeigh = (EditText) findViewById(R.id.editTextHeigh);
        EditTextAge = (EditText) findViewById(R.id.editTextAge);
        mDbHelper = new TrainingDbHelper(this);
        mDbHelper1 = new InformationDbHelper(this);
        mDbHelper2 = new OsmDbHelper(this);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Name = user.getEmail();
                } else {
                    Toast.makeText(SettingActivity.this, "Нет соединения с интернетом, или вы не авторизированы!", Toast.LENGTH_SHORT).show();
                }
            }
        };
        //displayDatabaseInfoTraining();
        //displayDatabaseInfoOsm();
        displayDatabaseInfo();
        mradioButtonM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gender = 1;
                mradioButtonM.setImageResource(R.drawable.mbutactive);
                mradioButtonW.setImageResource(R.drawable.wbutpas);

            }
        });
        mradioButtonW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gender = 2;
                mradioButtonW.setImageResource(R.drawable.wbutactive);
                mradioButtonM.setImageResource(R.drawable.mbutpas);

            }
        });
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateInf();
                displayDatabaseInfo();
                mStorageRef = FirebaseStorage.getInstance().getReference();
                riversRef = mStorageRef.child(Name + "/inf.db");
                upload();
                Intent intent = new Intent(SettingActivity.this, BasicActivity.class);
                startActivity(intent);
                finish();

            }
        });
    }
    /*
    public void displayDatabaseInfoTraining() {
        Cursor cursor = this.mDbHelper.getReadableDatabase().query(TrainingContract.Notes.TABLE_NAME, new String[]{"_id", TrainingContract.Notes.COLUMN_PULSE, TrainingContract.Notes.COLUMN_TIME, TrainingContract.Notes.COLUMN_ENERGY}, null, null, null, null, null);
        try {
            display.setText(cursor.getCount() + " измерений\n\n");
            display.append("Номер - Пульс - Время - Энергозатратность\n");
            int idColumnIndex = cursor.getColumnIndex("_id");
            int pulseColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_PULSE);
            int timeColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_TIME);
            int energyColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_ENERGY);
            while (cursor.moveToNext()) {
                int currentID = cursor.getInt(idColumnIndex);
                int currentPulse = cursor.getInt(pulseColumnIndex);
                int currentTime = cursor.getInt(timeColumnIndex);
                display.append("\n" + currentID + " - " + currentPulse + " уд - " + currentTime + " сек - " + cursor.getInt(energyColumnIndex) + " Кал ");
            }
        } finally {
            cursor.close();
        }
    }
    public void displayDatabaseInfoOsm() {
        Cursor cursor = this.mDbHelper2.getReadableDatabase().query(TrainingContract.OSM.TABLE_NAME2, new String[]{"_id", TrainingContract.OSM.COLUMN_PSIT, TrainingContract.OSM.COLUMN_PSTAND, TrainingContract.OSM.COLUMN_POINT, TrainingContract.OSM.COLUMN_ZONE}, null, null, null, null, null);
        try {
            display1.setText(cursor.getCount() + " измерений\n\n");
            display1.append("Номер - ЧСС сидя - ЧСС стоя - Балл - Зона\n");
            int idColumnIndex = cursor.getColumnIndex("_id");
            int psitColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_PSIT);
            int pstandColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_PSTAND);
            int pointColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_POINT);
            int zoneColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_ZONE);
            while (cursor.moveToNext()) {
                int currentID = cursor.getInt(idColumnIndex);
                int currentPsit = cursor.getInt(psitColumnIndex);
                int currentPstand = cursor.getInt(pstandColumnIndex);
                int currentPoint = cursor.getInt(pointColumnIndex);
                display1.append("\n" + currentID + " - " + currentPsit + " уд - " + currentPstand + " уд - " + currentPoint + " баллов - " + cursor.getInt(zoneColumnIndex) + " зона ");
            }
        } finally {
            cursor.close();
        }
    }
    */
    public void displayDatabaseInfo() {
        Cursor cursor = mDbHelper1.getReadableDatabase().query(TrainingContract.Information.TABLE_NAME1, new String[]{"_id", TrainingContract.Information.COLUMN_WEIGHT, TrainingContract.Information.COLUMN_HEIGH, TrainingContract.Information.COLUMN_AGE, TrainingContract.Information.COLUMN_GENDER}, null, null, null, null, "_id DESC");
        try {
            if (cursor.getCount() == 0) {
                insertInf();
                displayDatabaseInfo();
            }
            int idColumnIndex = cursor.getColumnIndex("_id");
            int weightColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_WEIGHT);
            int heighColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_HEIGH);
            int ageColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_AGE);
            int genderColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_GENDER);
            while (cursor.moveToNext()) {
                int currentID = cursor.getInt(idColumnIndex);
                int currentWeight = cursor.getInt(weightColumnIndex);
                int currentHeigh = cursor.getInt(heighColumnIndex);
                int currentAge = cursor.getInt(ageColumnIndex);
                int currentGender = cursor.getInt(genderColumnIndex);
                EditTextWeight.setText(currentWeight + "");
                EditTextHeigh.setText(currentHeigh + "");
                EditTextAge.setText(currentAge + "");
                if (currentGender == 1) {
                    mradioButtonM.setImageResource(R.drawable.mbutactive);
                    mradioButtonW.setImageResource(R.drawable.wbutpas);
                }
                if (currentGender == 2) {
                    mradioButtonW.setImageResource(R.drawable.wbutactive);
                    mradioButtonM.setImageResource(R.drawable.mbutpas);
                }
            }
        } finally {
            cursor.close();
        }
    }
    private void UpdateInf() {
        SQLiteDatabase db = this.mDbHelper1.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TrainingContract.Information.COLUMN_WEIGHT, Integer.valueOf(Integer.parseInt(this.EditTextWeight.getText().toString())));
        values.put(TrainingContract.Information.COLUMN_HEIGH, Integer.valueOf(Integer.parseInt(this.EditTextHeigh.getText().toString())));
        values.put(TrainingContract.Information.COLUMN_AGE, Integer.valueOf(Integer.parseInt(this.EditTextAge.getText().toString())));
        if (Gender==1) {
            values.put(TrainingContract.Information.COLUMN_GENDER, Integer.valueOf(1));
        }
        if (Gender==2) {
            values.put(TrainingContract.Information.COLUMN_GENDER, Integer.valueOf(2));
        }
        long newRowId = (long) db.update(TrainingContract.Information.TABLE_NAME1, values, null, null);
    }

    private void insertInf() {
        SQLiteDatabase db = this.mDbHelper1.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TrainingContract.Information.COLUMN_WEIGHT, Integer.valueOf(0));
        values.put(TrainingContract.Information.COLUMN_HEIGH, Integer.valueOf(0));
        values.put(TrainingContract.Information.COLUMN_AGE, Integer.valueOf(0));
        values.put(TrainingContract.Information.COLUMN_GENDER, Integer.valueOf(0));
        long newRowId = db.insert(TrainingContract.Information.TABLE_NAME1, null, values);
    }
    public void upload() {

        final Uri file = Uri.fromFile(new File(Environment.getDataDirectory(), "//data//" + "com.example.lukianmichael.WatchAndFit"
                + "//databases//" + "inf.db"));
        riversRef.putFile(file)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        Toast.makeText(SettingActivity.this, "Сохранено", Toast.LENGTH_SHORT).show();
                        //Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Toast.makeText(SettingActivity.this, "Ошибка загрузки данных!", Toast.LENGTH_SHORT).show();
                        // ...
                    }
                });
    }
}
