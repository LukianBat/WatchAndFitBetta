package com.example.lukianmichael.WatchAndFit;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.lukianmichael.WatchAndFit.data.InformationDbHelper;
import com.example.lukianmichael.WatchAndFit.data.TrainingContract;

public class BasicActivity extends AppCompatActivity {
    private ImageButton mButton;
    private ImageButton mButton1;
    private ImageButton mButton2;
    private Button mButton3;
    private TextView mTextViewWeight;
    private TextView mTextViewHeight;
    private TextView mTextViewAge;
    private TextView mTextViewGender;
    private InformationDbHelper mDbHelper1;
    FragmentTransaction fTrans;
    SettingActivity frag1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic);
        mButton1 = (ImageButton) findViewById(R.id.button8);
        mButton = (ImageButton) findViewById(R.id.button7);
        mButton2= (ImageButton) findViewById(R.id.button9);
        mTextViewWeight = (TextView) findViewById(R.id.textViewWeight);
        mTextViewHeight = (TextView) findViewById(R.id.textViewHeight);
        mTextViewAge = (TextView) findViewById(R.id.textViewAge);
        mTextViewGender = (TextView) findViewById(R.id.textViewGender);
        mDbHelper1 = new InformationDbHelper(this);
        mButton3= (Button)findViewById(R.id.button10);
        frag1 = new SettingActivity();
        displayDatabaseInfo();
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (BasicActivity.this, SettingActivity.class);
                startActivity(intent);
            }
        });
        mButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BasicActivity.this, RecycleViewActivityOsm.class);
                startActivity(intent);
            }
        });
        mButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (BasicActivity.this,RecycleViewActivityTr.class);
                startActivity(intent);
            }
        });
        mButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (BasicActivity.this,RecycleViewActivityKig.class);
                startActivity(intent);
            }
        });
    }
    public void displayDatabaseInfo() {
        Cursor cursor = mDbHelper1.getReadableDatabase().query(TrainingContract.Information.TABLE_NAME1, new String[]{"_id", TrainingContract.Information.COLUMN_WEIGHT, TrainingContract.Information.COLUMN_HEIGH, TrainingContract.Information.COLUMN_AGE, TrainingContract.Information.COLUMN_GENDER}, null, null, null, null, "_id DESC");
        try {
            int idColumnIndex = cursor.getColumnIndex("_id");
            int weightColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_WEIGHT);
            int heighColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_HEIGH);
            int ageColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_AGE);
            int genderColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_GENDER);
            while (cursor.moveToNext()) {
                int currentID = cursor.getInt(idColumnIndex);
                int currentWeight = cursor.getInt(weightColumnIndex);
                int currentHeigh = cursor.getInt(heighColumnIndex);
                int currentAge = cursor.getInt(ageColumnIndex);
                int currentGender = cursor.getInt(genderColumnIndex);
                mTextViewWeight.setText("Вес: "+currentWeight);
                mTextViewHeight.setText("Рост: "+currentHeigh);
                mTextViewAge.setText("Возраст: "+currentAge);
               if (currentGender == 1) {
                    mTextViewGender.setText("Пол: мужской");
                }
                if (currentGender == 2) {
                    mTextViewGender.setText("Пол: женский");
                }

            }
        } finally {
            cursor.close();
        }
    }
}
